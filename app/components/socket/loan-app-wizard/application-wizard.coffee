`import Ember from 'ember'`

AppWizardComponent = Ember.Component.extend

  #hideNavigator: Ember.computed.or('launch', 'popInFrontOfNav')

  page: 'launch'
  possiblePages: ['launch', 'summary', 'newSecurities', 'people', 'assets', 'liabilities', 'income', 'expenses', 'documents', 'finish']
  pages: Ember.computed('model.loanApplicationBlob.applicationType', ->
    possiblePages = @get('possiblePages').copy()
    appType = @get('model.loanApplicationBlob.applicationType')
    unless appType == 'New Loan'
      possiblePages.removeObject('newSecurities')
    return possiblePages
    )


  launch: Ember.computed.equal('page', 'launch')
  summary: Ember.computed.equal('page', 'summary')
  newSecurities: Ember.computed.equal('page', 'newSecurities')
  people: Ember.computed.equal('page', 'people')
  #properties: Ember.computed.equal('page', 'properties')
  assets: Ember.computed.equal('page', 'assets')
  liabilities: Ember.computed.equal('page', 'liabilities')
  income: Ember.computed.equal('page', 'income')
  expenses: Ember.computed.equal('page', 'expenses')
  documents: Ember.computed.equal('page', 'documents')
  finish: Ember.computed.equal('page', 'finish')

  progress: Ember.computed('pages', 'page', ->
    pages = @get('pages')
    page = @get('page')
    #pageCount = pages.get('length')
    pageCount = pages.get('length') - 1
    pageIndex = pages.indexOf(page)
    #progress = (pageIndex + 1) / pageCount
    progress = (pageIndex) / pageCount || 0
    return progress
    )



  actions:
    closeWizard: ->
      loanApplication = @get('model')
      loanApplication.save()
      @set('showWizard', false)
      return false

    launchWizard: ->
      self = @
      @set('page', 'launch')
      model = @get('model')
      model.util_UnblobBlob()
      loanParty = @get('model.loanParties.firstObject')
      @set('loanParty', loanParty)
      model.set('loanApplicationBlob.wizardStarted', true)
      #model.save()

      loanParties = model.get('loanParties')
      loanParties.forEach (party) ->
        party.get('clients').forEach (client) ->
          if client.get('email')
            if client.get('email').toLowerCase() == self.get('session.currentUser.email').toLowerCase()
              self.set('loanParty', party)

      @set('showWizard', true)
      return false


    navigate: (direction) ->
      currentPage = @get('page')
      pages = @get('pages')
      numberOfPages = pages.length
      pageIndex = pages.indexOf(currentPage)
      if direction == 'up'
        pageIndex = pageIndex - 1
      else
        pageIndex = pageIndex + 1
      newPage = pages[pageIndex]
      unless pageIndex == numberOfPages or pageIndex == -1
        @set('page', newPage)
      loanApplication = @get('model')
      loanApplication.save()
      return false


`export default AppWizardComponent`
