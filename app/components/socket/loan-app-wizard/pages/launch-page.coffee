`import Ember from 'ember'`

FlowSummaryPageComponent = Ember.Component.extend

  classNameBindings: [':application-wizard-page', 'show:in-focus']


  #applicationStates: ['started', 'submitted', 'pre approved', 'approved', 'unconditional', 'pending settlement', 'settled', 'finished', 'withdrawn', 'declined', 'deferred']
  actions:
    start: ->
      @attrs.start()
      return false


`export default FlowSummaryPageComponent`
