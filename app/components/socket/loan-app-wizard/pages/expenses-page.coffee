`import Ember from 'ember'`

ExpensesPageComponent = Ember.Component.extend

  classNameBindings: [':application-wizard-page', 'show:in-focus']

  actions:
    addExpense: ->
      loanParty = @get('model')
      newExpense = loanParty.addExpense()
      monthlyFrequency = @store.peekAll('frequency').findBy('name', 'Monthly')
      newExpense.set('frequency', monthlyFrequency)
      newExpense.set('showDetails', true)
      return false

    showExpenseTable: ->
      @set('showTable', true)
      return false


`export default ExpensesPageComponent`
