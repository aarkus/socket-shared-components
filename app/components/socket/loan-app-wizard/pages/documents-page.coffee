`import Ember from 'ember'`

AppWizardDocumentsPageComponent = Ember.Component.extend

  classNameBindings: [':application-wizard-page', 'show:in-focus']

  documentRequirements: Ember.computed('model.clients.@each.employmentStatus', 'model.clients.@each.clientType', 'model.clients.@each.countryOfBirth', 'model.incomes.@each.incomeType', ->
    documentRequirements = ['Bank statements covering AT LEAST the last 3 months transactions for all operating accounts', 'Identification (drivers license or passport) for each person', 'Proof of income for each income provided (employment contract, payslip, rental agreement/assessment)', 'Other documents as requested by your adviser']
    return documentRequirements
    )


  init: ->
    @_super()
    self = @
    self.set('gettingDocs', true)
    loanApplication = @get('model.loanApplication')
    loanApplication.get('documents').then ->
      self.set('gettingDocs', false)



`export default AppWizardDocumentsPageComponent`
