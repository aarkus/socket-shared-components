`import Ember from 'ember'`

ApplicationWizardPeoplePageComponent = Ember.Component.extend

  classNameBindings: [':application-wizard-page', 'show:in-focus']

  actions:
    addClient: (isPerson) ->
      if isPerson then clientType = 'Person' else clientType = 'Company'
      newClient = @store.createRecord('client',
        clientType: clientType
      )
      loanParty = @get('model')
      #loanApplication = loanParty.get('loanApplication')

      loanParty.addClient(newClient)
      newClient.set('showDetails', true)
      return false



    navigate: (direction) ->
      @attrs.navigate(direction)
      return false


`export default ApplicationWizardPeoplePageComponent`
