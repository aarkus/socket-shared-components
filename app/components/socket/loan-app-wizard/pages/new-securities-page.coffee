`import Ember from 'ember'`

FlowSummaryPageComponent = Ember.Component.extend

  classNameBindings: [':application-wizard-page', 'show:in-focus']

  actions:
    addSecurity: ->
      model = @get('model')
      clients = model.get('clients')
      newSecurity = @store.createRecord('security',
        isProposed: true
        clients: clients
      )
      model.get('loanApplication.securities').pushObject(newSecurity)
      newSecurity.set('showDetails', true)
      return false



`export default FlowSummaryPageComponent`
