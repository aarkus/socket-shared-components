`import Ember from 'ember'`

IncomesPageComponent = Ember.Component.extend

  classNameBindings: [':application-wizard-page', 'show:in-focus']

  actions:
    addIncome: ->
      loanParty = @get('model')
      newIncome = loanParty.addIncome()
      annualFrequency = @store.peekAll('frequency').findBy('name', 'Annually')
      newIncome.set('frequency', annualFrequency)
      newIncome.set('showDetails', true)
      return false



`export default IncomesPageComponent`
