`import Ember from 'ember'`

LiabilitiesPageComponent = Ember.Component.extend

  classNameBindings: [':application-wizard-page', 'show:in-focus']

  actions:
    addLiability: ->
      loanParty = @get('model')
      newLiability = loanParty.addLiability()
      newLiability.set('showDetails', true)
      return false



`export default LiabilitiesPageComponent`
