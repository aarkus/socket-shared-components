`import Ember from 'ember'`

AppWizardFinishPageComponent = Ember.Component.extend

  classNameBindings: [':application-wizard-page', 'show:in-focus']

  actions:
    close: ->
      model = @get('model')
      loanApplication = model.get('loanApplication')
      loanApplicationBlob = loanApplication.get('loanApplicationBlob')
      loanApplicationBlob.set('wizardComplete', true)
      loanApplication.save()
      @set('page', 'summary')
      @set('closeAttribute', false)

`export default AppWizardFinishPageComponent`
