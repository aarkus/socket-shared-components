`import Ember from 'ember'`

AppWizardSummaryPageComponent = Ember.Component.extend

  classNameBindings: [':application-wizard-page', 'show:in-focus']

  hideApplicationValue: Ember.computed.equal('appType', 'I wish to move my existing lending to a new lender')

  appTypes: Ember.computed.mapBy('appTypeOptions', 'display')
  appType: Ember.computed('model.loanApplication.loanApplicationBlob.applicationType',
    get: (key) ->
      value = @get('model.loanApplication.loanApplicationBlob.applicationType')
      appTypeOptions = @get('appTypeOptions')
      appType = appTypeOptions.findBy('value', value)
      if appType
        return appType.display
      else
        return null

    set: (key, value) ->
      appTypeOptions = @get('appTypeOptions')
      appType = appTypeOptions.findBy('display', value).value
      @set('model.loanApplication.loanApplicationBlob.applicationType', appType)
      if appType == 'Refinance'
        @set('model.loanApplication.applicationValue', null)
      return value
    )


  appTypeOptions: [
    {
      display: "I wish to borrow money to purchase a new property(s)"
      value: 'New Loan'
    }, {
      display: "I wish to borrow more money against my existing property(s) (topup)"
      value: 'Topup'
    }, {
      display: "I wish to move my existing lending to a new lender"
      value: 'Refinance'
    }
  ]

`export default AppWizardSummaryPageComponent`
