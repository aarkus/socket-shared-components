`import Ember from 'ember'`

AppWizardSecuritiesPageComponent = Ember.Component.extend

  classNameBindings: [':application-wizard-page', 'show:in-focus']

  actions:
    addSecurity: ->
      loanParty = @get('model')
      newSecurity = loanParty.addSecurity()
      newSecurity.set('showDetails', true)
      return false




`export default AppWizardSecuritiesPageComponent`
