`import Ember from 'ember'`

AppWizardAssetPageComponent = Ember.Component.extend

  classNameBindings: [':application-wizard-page', 'show:in-focus']

  actions:
    addAsset: ->
      loanParty = @get('model')
      newAsset = loanParty.addAsset()
      newAsset.set('showDetails', true)
      return false


`export default AppWizardAssetPageComponent`
