`import Ember from 'ember'`
`import {countries, countriesWithoutZip, countriesWithState} from 'ember-countries';`

FlowSummaryPageComponent = Ember.Component.extend

  notify: Ember.inject.service('notify')

  classNames: ['application-wizard-client-details']
  showDetails: false

  genders: ['', 'male', 'female', 'other']

  test: 'test'

  isPerson: Ember.computed('model.clientType', ->
    unless @get('model.clientType')
      return true
    unless @get('model.clientType') == 'Company' || @get('model.clientType') == 'Trust'
      return true
    )

  showCopyPerson: Ember.computed.and('isPerson', 'individualsToCopyFrom')


  individualsToCopyFrom: Ember.computed('loanParty.clients.[]', ->
    clientsList = []
    model = @get('model')
    if @get('loanParty.clients.length') > 0
      @get('loanParty.clients').forEach (client) ->
        unless model == client
          clientsList.pushObject(client)
    return clientsList
    )


  previousAddressRequired: Ember.computed('model.monthsAtCurrentAddress', ->
    if @model.get('monthsAtCurrentAddress') < 36
      return true
    )

  previousEmployerRequired: Ember.computed('model.monthsAtCurrentEmployer', ->
    if @model.get('monthsAtCurrentEmployer') < 36
      return true
    )



  actions:
    copyIndividual: (individual) ->
      notify = @get('notify')
      self = @
      model = @get('model.content') || @get('model')
      model.copyClient(individual)
      notify.success 'Copied!'
      @set('showCopyClients', false)


    delete: ->
      @set('model.showDetails', false)
      model = @get('model.content') || @get('model')
      loanParty = @get('loanParty')
      loanParty.removeClient(model)
      return false

    save: ->
      self = @
      @set('isSaving', true)
      model = @get('model.content') || @get('model')
      model.save().then (model) ->
        la = self.get('loanParty.loanApplication')
        la.save()
        self.set('isSaving', false)
        self.set('model.showDetails', false)
      return false

    viewDetails: ->
      @set('model.showDetails', true)
      return false

    viewCopyClients: ->
      @toggleProperty('showCopyClients')
      return false



  init: ->
    countriesList = countries.mapBy('country')
    countriesList.pushObject(null)
    @set 'countriesList', countriesList

    @_super()






`export default FlowSummaryPageComponent`
