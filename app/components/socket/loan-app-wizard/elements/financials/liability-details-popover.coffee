`import Ember from 'ember'`
`import AddSaveDeleteFinancialsMixin from '../../../../../mixins/socket/add-save-delete-financials-mixin'`

`import LoanCalculationsMixin from '../../../../../mixins/shared/loan-calculations-mixin'`

LiabilityDetailsPopoverComponent = Ember.Component.extend(AddSaveDeleteFinancialsMixin, LoanCalculationsMixin,

  classNames: ['s24 flex-column-nowrap flex-align-centre']

  isPartialRepay: Ember.computed.equal('model.liabilitySettlementAction', 'Partially Repaid')

  showRepayments: Ember.computed('model.liabilityType', ->
    if @get('model.requiresTerm')
      return true
    if ['Hire purchase', 'Store card', 'Other loan'].includes(@get('model.liabilityType.name'))
      return true
    )

  settlementActions: Ember.computed('model.liabilityType', ->
    actionsList = @get('actionsList').copy()
    refinanceOption = actionsList.findBy('value', 'Kept (will refinance if lender changes)')
    unless @get('model.liabilityType.isSecured')
      actionsList.removeObject(refinanceOption)
    return actionsList.mapBy('display')
    )


  settlementAction: Ember.computed('model.loanPartyLiabilityLink.liabilitySettlementAction',
    get: (key) ->
      value = @get('model.loanPartyLiabilityLink.liabilitySettlementAction')
      actions = @get('actionsList')
      action = actions.findBy('value', value)
      if action
        return actions.findBy('value', value).display
      else
        return null

    set: (key, value) ->
      actions = @get('actionsList')

      action = actions.findBy('display', value).value
      @set('model.loanPartyLiabilityLink.liabilitySettlementAction', action)
      return value
    )

  termCalculator: Ember.observer('model.interestRate', 'model.limit', 'model.interestOnly', 'model.requiresTerm', 'model.repayments', 'model.frequency', ->
    rate = @get('model.interestRate')
    limit = @get('model.limit')
    intOnly = @get('model.interestOnly')
    requiresTerm = @get('model.requiresTerm')
    repayments = @get('model.repayments')
    frequency = @get('model.frequency')

    if intOnly or !requiresTerm or !repayments or !frequency or !rate or !limit then return false

    pmtsPerYear = frequency.get('yearConversion')
    term = @calculateRemainingTerm(limit, rate, repayments, pmtsPerYear)
    @set('model.remainingMonths', term)
    )


  actionsList: [
    {
    value: 'Kept (will not refinance)'
    display: 'It will not change'
    },
    {
    value: 'Kept (will refinance if lender changes)'
    display: 'If the new lender is different, it will move to the new lender'
    },
    {
    value: 'Fully Repaid'
    display: 'It will be paid off in full'
    },
    {
    value: 'Partially Repaid'
    display: 'It will be partially paid off'
    }
  ]

)

`export default LiabilityDetailsPopoverComponent`
