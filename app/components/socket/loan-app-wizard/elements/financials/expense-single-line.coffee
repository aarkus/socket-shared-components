`import Ember from 'ember'`
`import TableRowOptionsMixin from '../../../../../mixins/socket/table-options-mixin'`
`import AddSaveDeleteFinancialsMixin from '../../../../../mixins/socket/add-save-delete-financials-mixin'`

BorrowersIndividualTableRowComponent = Ember.Component.extend(TableRowOptionsMixin, AddSaveDeleteFinancialsMixin,

  classNames: ['s24 flex-row-nowrap flex-align-centre table-single-line']


)

`export default BorrowersIndividualTableRowComponent`
