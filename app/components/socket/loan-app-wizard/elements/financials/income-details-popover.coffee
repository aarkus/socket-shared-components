`import Ember from 'ember'`
`import AddSaveDeleteFinancialsMixin from '../../../../../mixins/socket/add-save-delete-financials-mixin'`

IncomeDetailsPopoverComponent = Ember.Component.extend(AddSaveDeleteFinancialsMixin,

  classNames: ['s24 flex-column-nowrap flex-align-centre']

  
  showNetGross: Ember.computed.alias('model.incomeType.isTaxedAtPersonalRate')
)

`export default IncomeDetailsPopoverComponent`
