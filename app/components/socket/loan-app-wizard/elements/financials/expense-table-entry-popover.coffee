`import Ember from 'ember'`

AssetDetailsPopoverComponent = Ember.Component.extend(

  remainingExpenses: Ember.computed('model.@each.expenseType', ->
    expenseTypes = @store.peekAll('expenseType').sortBy('sortOrder')
    usedTypes = @get('model').mapBy('expenseType')
    remainingExpenses = []
    monthlyFrequency = @store.peekAll('frequency').findBy('name', 'Monthly')
    expenseTypes.forEach (type) ->
      unless usedTypes.includes(type)
        remainingExpense = {
          expenseType: type
          value: null
          frequency: monthlyFrequency
        }
        remainingExpenses.pushObject(remainingExpense)
    return remainingExpenses

    )

  actions:
    viewTable: ->
      @set('showTable', true)
      return false

    saveExpenses: ->
      self = @
      @set('isSaving', true)
      party = @get('party')
      loanApplication = party.get('loanApplication')
      clients = party.get('clients')
      remainingExpenses = @get('remainingExpenses')
      expenseSaves = []
      remainingExpenses.forEach (expense) ->
        if expense.value > 0
          newExpense = self.store.createRecord('expense',
            expenseType: expense.expenseType
            frequency: expense.frequency
            value: expense.value
            clients: clients
          )
          expenseSaves.pushObject(newExpense.save())
          party.get('expenses').pushObject(newExpense)
      Ember.RSVP.all(expenseSaves).then (->
        self.set('showTable', false)
        self.set('isSaving', true)
        return false
      ), (error) ->
        self.set('isSaving', true)
        self.set('showTable', false)
        return false


)

`export default AssetDetailsPopoverComponent`
