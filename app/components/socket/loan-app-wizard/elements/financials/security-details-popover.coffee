`import Ember from 'ember'`
`import AddSaveDeleteFinancialsMixin from '../../../../../mixins/socket/add-save-delete-financials-mixin'`

AssetDetailsPopoverComponent = Ember.Component.extend(AddSaveDeleteFinancialsMixin,

  classNames: ['s24 flex-column-nowrap flex-align-centre']



  settlementActions: Ember.computed('bank', ->
    actions = ['It will be sold']
    if @get('bank') == 'No Mortgage'
      actions.pushObjects(['It will be kept without mortgage', 'It will be provided as security to the new lender'])
    else
      actions.pushObjects(['It will be provided as security to the new lender', 'It will remain as security with the existing lender'])
    return actions
    )

  settlementAction: Ember.computed('model.loanPartySecurityLink.securitySettlementAction', 'bank',
    get: (key) ->
      actions = @get('actionList')
      value = @get('model.loanPartySecurityLink.securitySettlementAction')
      if @get('bank') == 'No Mortgage'
        action = actions.findBy('value', value)
        if action
          return action.withoutMortgage
        else
          return null

      else
        action = actions.findBy('value', value)
        if action
          return actions.findBy('value', value).withMortgage
        else
          return null

    set: (key, value) ->
      actions = @get('actionList')
      if @get('bank') == 'No Mortgage'
        action = actions.findBy('withoutMortgage', value).value
      else
        action = actions.findBy('withMortgage', value).value
      @set('model.loanPartySecurityLink.securitySettlementAction', action)
      return value
    )


  bankNames: []
  bank: null
  bankObserver: Ember.observer('bank', ->
    bankName = @get('bank')
    if bankName == 'No Mortgage'
      @set('model.bank', null)
    else
      bank = @store.peekAll('bank').findBy('name', bankName)
      @set('model.bank', bank)
    )

  actions:
    customUndo: ->
      model = @get('model.content') || @get('model')
      address = model.get('address.content')
      address.rollbackAttributes()
      model.rollbackAttributes()
      return false




  init: ->
    @_super()
    bankNames = @store.peekAll('bank').mapBy('name')
    bankNames.pushObject('No Mortgage')
    @set('bankNames', bankNames)
    if @get('model.bank.id')
      bank = @get('model.bank.name')
      @set('bank', bank)
    else
      @set('bank', 'No Mortgage')



  actionList: [
    {
    value: 'Sold'
    withMortgage: 'It will be sold'
    withoutMortgage: 'It will be sold'
    },
    {
    value: 'Kept (will refinance if lender changes)'
    withMortgage: 'It will be provided as security to the new lender'
    withoutMortgage: 'It will be provided as security to the new lender'
    },
    {
    value: 'Kept (will not refinance)'
    withMortgage: 'It will remain as security with the existing lender'
    withoutMortgage: 'It will be kept without mortgage'
    }
  ]
)

`export default AssetDetailsPopoverComponent`
