`import Ember from 'ember'`

FlowSummaryPageComponent = Ember.Component.extend

  #classNameBindings: [':wizard-page', 'show:in-focus']

  classNames: ['application-wizard-navigation-container']

  actions:
    navigate: (direction) ->
      @attrs.navigate(direction)
      return false




  didRender: ->
    @_super()
    @drawNavigator()


  drawNavigator: ->
    progress = @get('progress') * 2
    canvas = document.getElementById('navCanvas')
    context = canvas.getContext('2d')
    context.clearRect(0, 0, 70, 70)
    context.beginPath()
    context.arc(35, 35, 30, 0, 2 * Math.PI)
    context.closePath()
    context.lineWidth = 4
    context.strokeStyle = '#DCDCDC'
    context.stroke()

    start = 1.5 * Math.PI
    end = start + progress * Math.PI

    context.beginPath()
    context.arc(35, 35, 30, start, end, false)
    context.arc(35, 35, 30, end, start, true)
    context.closePath()
    context.lineWidth = 4
    context.strokeStyle = '#41ACCC'
    context.stroke()

    context.beginPath()
    context.moveTo(10, 35)
    context.lineTo(60, 35)
    context.stroke()

    context.lineWidth = 1

    context.fillStyle = '#41ACCC'

    unless progress == 0
      context.beginPath()
      context.moveTo(35, 10)
      context.lineTo(55, 27)
      context.lineTo(15, 27)
      context.closePath()
      context.fill()

    unless progress == 2
      context.beginPath()
      context.moveTo(35, 60)
      context.lineTo(55, 43)
      context.lineTo(15, 43)
      context.closePath()
      context.fill()



`export default FlowSummaryPageComponent`
