`import Ember from 'ember'`

BorrowersIndividualTableRowComponent = Ember.Component.extend(

  classNames: ['s24 flex-row-nowrap flex-align-centre table-single-line']

  documentPurpose: Ember.computed('model', ->
    model = @get('model')
    switch model
      when 'Bank statements covering AT LEAST the last 3 months transactions for all operating accounts'
        purpose = 'Bank statements'
      when 'Identification (drivers license or passport) for each person'
        purpose = 'Identification'
      when 'Proof of income for each income provided (employment contract, payslip, rental agreement/assessment)'
        purpose = 'Proof of income'
      else
        return null
    docPurpose = @store.peekAll('documentPurpose').findBy('name', purpose)
    return docPurpose
    )


  documents: Ember.computed('loanApplication.documents.content.@each.documentPurpose', ->
    documentPurpose = @get('documentPurpose')
    docs = @get('loanApplication.documents.content').filterBy('documentPurpose', documentPurpose)
    return docs
    )

  hasDocs: Ember.computed.gt('documents.length', 0)

  uploadingDocs: Ember.computed('documents.@each.isUploading', ->
    @get('documents').filterBy('isUploading')
    )


  uploading: Ember.computed('uploadingDocs.@each.uploadProgress', ->
    uploadingDocs = @get('uploadingDocs')
    if uploadingDocs.length == 0
      return false
    uploading = 0
    uploadingDocs.forEach (doc) ->
      uploading += doc.get('uploadProgress')
    progress = uploading / uploadingDocs.length
    return progress
    )



  actions:
    viewDocs: ->
      @set('showDocs', true)
      return false

    newDocumentCreated: (doc) ->
      loanParty = @get('loanParty')
      loanApplication = loanParty.get('loanApplication')
      clients = loanParty.get('clients')
      #TODO: make sure getting clients in time to set
      doc.get('loanApplications').pushObject(loanApplication)
      purpose = @get('documentPurpose')
      doc.set('documentPurpose', purpose)
      loanApplication.get('documents').pushObject(doc)
      return false


    newDocumentUploadComplete: (doc) ->
      loanParty = @get('loanParty')
      loanApplication = loanParty.get('loanApplication')
      clients = loanParty.get('clients')
      clients.forEach (client) ->
        doc.get('clients').pushObject(client)
      #loanApplication.get('documents').pushObject(doc)
      loanApplication.save().then (la) ->
        doc.save()

      return false


    uploadError: ->
      notify = @get('notify')
      notify.error('There was a problem uploading one of your documents. Please try again')
      return false

)

`export default BorrowersIndividualTableRowComponent`
