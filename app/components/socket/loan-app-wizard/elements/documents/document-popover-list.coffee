`import Ember from 'ember'`

BorrowersIndividualTableRowComponent = Ember.Component.extend(

  classNames: ['s24 flex-row-nowrap flex-align-centre table-single-line']

  lastModifiedDisplay: Ember.computed('lastModified', ->
    return 'last modified ' + @model.get('lastModified')
    )

  actions:
    viewDocument: ->
      model = @get('model.content') || @get('model')
      unless model.get('isUploading')
        model.viewDocument()
      return false

)

`export default BorrowersIndividualTableRowComponent`
