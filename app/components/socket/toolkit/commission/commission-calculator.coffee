`import Ember from 'ember'`

ToolkitCommissionCommissionCalculatorComponent = Ember.Component.extend(

  classNames: ['s24 flex-row-wrap flex-justify-centre whitesmoke-bg pricing-comparator-body']

  session: Ember.inject.service('session')


  noTrail: Ember.computed.equal('model.totalTrail', 0)

  actions:
    addLoan: ->
      monthlyFrequency = @store.peekAll('frequency').findBy('name', 'Monthly')
      newLoan = @store.createRecord('liability',
        owing: 100000
        interestRate: 0.05
        remainingMonths: 360
        frequency: monthlyFrequency
      )
      @model.get('liabilities').pushObject(newLoan)
      Ember.run.later(this, ->
        newLoan.set 'showLoan', true
      , 200)

    reset: ->
      @get('model.liabilities').clear()
      return false


  init: ->
    @_super()
    unless @get('session.savedTool')
      @set('showInstructions', true)

)

`export default ToolkitCommissionCommissionCalculatorComponent`
