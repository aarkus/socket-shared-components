`import Ember from 'ember'`
`import accounting from 'accounting'`

ToolkitCommissionCommissionForecastGraphComponent = Ember.Component.extend(

  classNames: ['s24']

  dataObserver: Ember.observer('model.monthlyCommissionPayments',  ->
    Ember.run.debounce(this, 'updateLineGraph', 400)
  )


  didRender: ->
    @_super()
    @set 'plannerComponent', this
    unless @get('lineChart')
      Ember.run.later(this, ->
        @initBaselineGraph()
        2000
        )
      Ember.run.later(this, ->
        @updateLineGraph()
        3000
        )


  initBaselineGraph: ->
    #self = @
    labels = []
    data = []

    lineChartData = {
      labels: labels
      datasets: [
        {
          lineTension: 0.05
          label: 'commission'
          data: data #runningBalancesBaseline
          borderColor: 'rgba(72, 160, 220, 1.00)'
          backgroundColor: 'rgba(0, 0, 220, 0.0)'
        }
      ]
    }
    if @get('print')
      ctx = document.getElementById("lineChartPrint").getContext("2d")
    else
      ctx = document.getElementById("lineChart").getContext("2d")

    lineChart = new Chart(ctx,
      type: 'line'
      data: lineChartData
      options:
        responsive: true
        maintainAspectRatio: false
        events:
          [
            #'dragover'
            'mousemove'
            'mouseout'
            'click'
            'touchstart'
            'touchmove'
            'touchend'
          ]
        elements:
          point: {
            hitRadius: 1,
            radius: 1
          }
        scales: {
          xAxes: [
            scaleLabel:
              display: true
              labelString: 'Year'
            ticks: {
              display: true
              autoSkip: false
              callback: (value, index, values) ->
                  if value % 12 == 0
                    return Math.floor(value / 12)
                  else
                    return ''
                  #return accounting.formatMoney(value, '$', 0)
              #fixedStepSize: 12
              #stepSize: 12

              }
            gridLines: {
              drawOnChartArea: false
              drawTicks: false
            }
          ]
          yAxes: [
            scaleLabel:
              display: true
              labelString: 'Approx monthly trail'
            ticks: {
              callback: (value, index, values) ->
                  return accounting.formatMoney(value, '$', 2)
              display: true
              beginAtZero: true
              min: 0
              }
          ]
        }
        legend: {display: false}
        hover: {mode: 'single'}
        tooltips:
          mode: 'label'
          callbacks:
            title: (tooltipItem, data) ->
              month = tooltipItem[0].index + 1
              year = Math.floor(month / 12)
              resMonth = month % 12
              return "Year #{year}, Month #{resMonth}"
            label: (tooltipItem, data) ->
              label = tooltipItem.yLabel || 0
              return "Trail: #{accounting.formatMoney(label, '$', 2)}"
              #if tooltipItem.datasetIndex == 0
              #  type = 'Plan: '
              #else
              #  type = 'Baseline: '
              #label = tooltipItem.yLabel || 0
              #return type +  accounting.formatMoney(label, '$', 2)
    )

    @set 'lineChart', lineChart


  updateLineGraph: ->
    commissionPayments = @get('model.monthlyCommissionPayments')
    commissionPayments.shift()

    lineChart = @get 'lineChart'
    labels = []
    i = 1
    longestLength = commissionPayments.length

    while i < longestLength
      labels.pushObject(i)
      i++
    lineChart.data.labels = labels
    lineChart.data.datasets[0].data = commissionPayments
    lineChart.update()
)

`export default ToolkitCommissionCommissionForecastGraphComponent`
