`import Ember from 'ember'`
`import accounting from 'accounting'`
`import GeneratePDFMixin from '../../../../mixins/socket/generate-pdf-mixin'`
`import PdfUtilitiesMixin from '../../../../mixins/shared/pdf-utility-functions'`


PricingPricingComparatorComponent = Ember.Component.extend(GeneratePDFMixin, PdfUtilitiesMixin,

  classNames: ['s24 pad5 whitesmoke-bg pricing-comparator-body flex-column-nowrap']

  tutorial: null
  title: 'Offer Comparison'

  utilityButtons: Ember.computed('model.loanStructure.loanStructureLiabilities.[]', 'model.pricingOffers.[]', ->
    functions = [
      {
        iconPath: 'icons/buttons/save'
        action: 'save'
        title: 'Save'
      },
      {
        iconPath: 'icons/buttons/share'
        action: 'share'
        title: 'Share'
      }
    ]
    if @get('model.loanStructure.loanStructureLiabilities.length') > 0 && @get('model.pricingOffers.length') > 0
      printFunction = {
        iconPath: 'icons/buttons/print'
        action: 'print'
        title: 'Print'
      }
      functions.unshiftObject(printFunction)
    return functions
    )
    ###
    [
    {
      iconPath: 'icons/buttons/save'
      action: 'save'
      title: 'Save'
    },
    {
      iconPath: 'icons/buttons/print'
      action: 'print'
      title: 'Print'
    },
    {
      iconPath: 'icons/buttons/share'
      action: 'share'
      title: 'Share'
    }
  ]
  ###

  actions:
    save: ->
      console.log 'save'

    print: ->
      @createPDF()
      return false

    share: ->
      console.log 'share'

    viewInstructions: ->
      @set('showInstructions', true)
      #@createPDF()
      return false

    saveOffer: (offer) ->
      offer.set('showDetails', false)
      return false



  createPDF: ->
    headerColour = @get('headerColour')
    title = @get('title')
    self = @
    doc = @setupPDF(headerColour, title)
    rowY = 30
    textY = 32
    fontSize = 11
    doc.setTextColor(116, 118, 120)
    doc.setFontSize(fontSize)
    self.util_CentredText(doc, "Proposed Loan Structure" , fontSize, textY)
    rowY += 10
    textY += 10

    $('#structureDonut').addClass('structure-donut-print')
    structureDonut = document.getElementById('structureDonut')
    structureDonutImage = structureDonut.toDataURL('image/png')

    if window.devicePixelRatio > 1
      doc.addImage structureDonutImage, 'JPEG', 6, 40, 80, 60
    else
      doc.addImage structureDonutImage, 'JPEG', 6, 40

    midSection = 70
    loanStructureLiabilityRowHeight = 6
    fontSize = 9
    loanStructureLiabilities = self.get('model.loanStructure.loanStructureLiabilities').sortBy('fixedRateMonths')
    numberOfLoanStructureLiabilities = loanStructureLiabilities.get('length')
    topOfRow = midSection - ( numberOfLoanStructureLiabilities * loanStructureLiabilityRowHeight / 2 )
    i = 1

    loanStructureLiabilities.forEach (lia) ->
      if i % 2 == 0
        doc.setFillColor(240, 240, 240)
        doc.rect(95, topOfRow, 105, loanStructureLiabilityRowHeight, 'F')

      loanAmount = accounting.formatMoney(lia.get('owing'), '$', 0)
      liabilityType = lia.get('typeSummary') || 'unknown loan type'
      if lia.get('interestOnly') then liabilityType = liabilityType + " (int only)"

      doc.setTextColor(82, 86, 89)
      doc.setFontSize(fontSize)
      doc.text(96, topOfRow + 4, liabilityType)
      self.rightAlignedText(doc, loanAmount, 197, topOfRow + 4)

      topOfRow += loanStructureLiabilityRowHeight
      i++

    rowY = 120
    textY = 122

    doc.setTextColor(116, 118, 120)
    self.util_CentredText(doc, "Rates/Offers" , 11, textY)

    rowY += 5
    textY += 5
    doc.setFontSize(8)
    termsWithRates = self.util_ReturnTermsWithRates()
    termWidth = 15
    rowWidth = 60 + termsWithRates.length  * termWidth
    leftX = 105 - ( rowWidth / 2 )

    termsX = leftX + 30 #20 for the icon, then 10 to be centred on next cell
    self.util_CentredOnXText(doc, "Cash", 8, termsX, textY)
    termsX += 20 #20 for the cash
    self.util_CentredOnXText(doc, "Monthly Fees", 8, termsX, textY)
    termsX += 20 #20 for the fees



    termsWithRates.forEach (term) ->
      self.util_CentredOnXText(doc, term, 8, termsX, textY)
      termsX += termWidth

    rowY += 5
    textY += 5

    i = 1
    @get('model.pricingOffers').forEach (offer) ->
      if i % 2 == 0
        doc.setFillColor(240, 240, 240)
        doc.rect(leftX, rowY, rowWidth, 7, 'F')
      bankIconImgPath = offer.get('bank.logoPath')
      bankIcon = $("img[src$='#{bankIconImgPath}']")[0]
      doc.addImage bankIcon, 'PNG', leftX + 10, rowY + 1, 5, 5
      doc.setTextColor(82, 86, 89)
      doc.setFontSize(fontSize)
      valuesX = leftX + 30
      rowY += 2
      textY += 2
      cashContribution = offer.get('cashContribution') || 0
      self.util_CentredOnXText(doc, accounting.formatMoney(cashContribution , '$', 0), fontSize, valuesX, textY)
      valuesX += 20
      monthlyFees = offer.get('monthlyFees') || 0
      self.util_CentredOnXText(doc, accounting.formatMoney(monthlyFees, '$', 2), fontSize, valuesX, textY)
      valuesX += 20
      termsWithRates.forEach (term) ->
        rate = self.util_MatchRateForTerm(offer, term)
        self.util_CentredOnXText(doc, rate, fontSize, valuesX, textY)
        valuesX += termWidth
      rowY += 5
      textY += 5
      i++

    rowY += 15
    textY += 15
    doc.setTextColor(116, 118, 120)
    self.util_CentredText(doc, "Comparison" , 11, textY)

    rowY += 5
    textY += 5

    pmtLabel = "#{@get('model.frequency.name')} Repayments"
    self.util_CentredOnXText(doc, pmtLabel, 8, 80, textY)
    self.util_CentredOnXText(doc, "5yr Int + Fees", 8, 130, textY)
    self.util_CentredOnXText(doc, "Lifetime Int + Fees", 8, 180, textY)
    #self.util_CentredOnXText(doc, "Customer Sat", 8, 80, textY)
    rowY += 5
    textY += 5

    i = 1
    @get('model.comparisons').forEach (comparison) ->
      if comparison.repayments
        if i % 2 == 0
          doc.setFillColor(240, 240, 240)
          doc.rect(10, rowY, 190, 7, 'F')
        bankIconImgPath = comparison.bank.get('logoPath')
        bankIcon = $("img[src$='#{bankIconImgPath}']")[0]
        doc.addImage bankIcon, 'PNG', 27, rowY + 1, 5, 5
        doc.setFontSize(fontSize)
        rowY += 2
        textY += 2
        doc.setTextColor(82, 86, 89)
        self.util_CentredOnXText(doc, accounting.formatMoney( comparison.repayments, "$", 0 ), fontSize, 80, textY)
        self.util_CentredOnXText(doc, accounting.formatMoney( comparison.fiveYrCost, "$", 0 ), fontSize, 130, textY)
        self.util_CentredOnXText(doc, accounting.formatMoney( comparison.lifetimeCost, "$", 0 ), fontSize, 180, textY)
        rowY += 5
        textY += 5
        i++

    doc.save("offerComparison.pdf")
    $('#structureDonut').removeClass('structure-donut-print')



  util_ReturnTermsWithRates: ->
    terms = []
    allRates = []
    @get('model.pricingOffers').forEach (pricingOffer) ->
      rates = pricingOffer.get('pricingOfferRates.content')
      rates.forEach (rate) ->
        if rate.get('interestRate')
          allRates.pushObject(rate)
    allRates.sortBy('fixedRateMonths').forEach (term) ->
      typeSummary = term.get('typeSummary')
      unless terms.includes(typeSummary)
        terms.pushObject(typeSummary)
    return terms


  util_MatchRateForTerm: (offer, term) ->
    matchingRate = offer.get('pricingOfferRates').findBy('typeSummary', term)
    if matchingRate.get('interestRate') > 0
      rate = ( matchingRate.get('interestRate') * 100 ).toFixed(2)
      return rate + '%'
    else
      return '-'



  init: ->
    @_super()
    self = @
    offerComparatorTutorial = @get('offerComparatorTutorial')
    release = @store.createRecord('release',
        title: offerComparatorTutorial.title
      )
    offerComparatorTutorial.demoPages.forEach (page) ->
      item = self.store.createRecord('helpItem',
        title: page.title
        description: page.description
        imagePath: page.imagePath
        videoPath: page.videoPath
        release: release
      )
      release.get('helpItems').pushObject(item)
    @set('tutorial', release)


  offerComparatorTutorial: {
    title: 'Socket Offer Comparator'
    demoPages: [
      {
        title: "Introduction"
        imagePath: "https://s3-ap-southeast-2.amazonaws.com/socket-public/help-images/tutorial-icon.png"
        description: "Socket's Offer Comparator helps you objectively compare rates/offers from multiple lenders by calculating the repayments and costs associated with each offer for your loan structure. Customer Satisfaction will soon be provided too!"
      },
      {
        title: "1. Record your loan structure"
        description: "First, record your current or proposed loan structure in the loan structure section. <br/>Socket will calculate comparison figures based on your specific loan structure."
        imagePath: 'https://s3-ap-southeast-2.amazonaws.com/socket-public/help-images/la/0.0.2+-+Intro+Tutorials/loan-app-dashboard.png'
        videoPath: ''
      },
      {
        title: "2. Record lender's rates "
        description: "Record the rates/offers you have from different lenders, or import rates using the 'import' button.  <br/>For a more accurate comparison, record any cash incentive offered by each lender, and the total monthly fees that you'll pay with that lender (e.g. monthly account fees)"
        imagePath: 'https://s3-ap-southeast-2.amazonaws.com/socket-public/help-images/la/0.0.2+-+Intro+Tutorials/loan-app-create-application.png'
        videoPath: ''
      },
      {
        title: "3. Compare"
        description: "View the repayments and costs associated with each lender.<br/> <b>Tip:</b> Tap/Click on the 'Monthly Repayment' column header to toggle a different frequency"
        imagePath: "https://s3-ap-southeast-2.amazonaws.com/socket-public/help-images/la/0.0.2+-+Intro+Tutorials/loan-app-client-invite.png"
        videoPath: ""
      },
      {
        title: "Notes on our comparison calculations"
        description: "To provide the most realistic figures for comparison, Socket assumes the long term average interest rate for all loans will be 6.5%p.a. <br/> When calculating total costs, we apply this rate after 2yrs for revolving and floating loans, and after the fixed rate period for fixed loans."
        imagePath: "https://s3-ap-southeast-2.amazonaws.com/socket-public/help-images/la/0.0.2+-+Intro+Tutorials/loan-app-approval-new.png"
        videoPath: ""
      }

    ]
  }


)

`export default PricingPricingComparatorComponent`
