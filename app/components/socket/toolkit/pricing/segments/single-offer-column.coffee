`import Ember from 'ember'`

PricingSegmentsSingleOfferColumnComponent = Ember.Component.extend(

  classNameBindings: [':s08', ':m04', ':flex-column-nowrap', ':pricing-bottom-border', 'pricingOfferGreyColumn::pricing-offer-grey-column']
  
  pricingOfferGreyColumn: Ember.computed('index', ->
    index = @get('index')
    if index % 2 == 0
      return true
    )

  rateSorting: ['liability.liabilityType.sortOrder', 'liability.fixedRateMonths']
  sortedRates: Ember.computed.sort('model.pricingOfferRates', 'rateSorting')

)

`export default PricingSegmentsSingleOfferColumnComponent`
