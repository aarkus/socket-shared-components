`import Ember from 'ember'`

PricingSegmentsSingleOfferComparisonComponent = Ember.Component.extend(

  classNameBindings: [':s08', ':m04', ':flex-column-nowrap', ':pricing-top-border', 'pricingOfferGreyColumn::pricing-offer-grey-column']

  pricingOfferGreyColumn: Ember.computed('index', ->
    index = @get('index')
    if index % 2 == 0
      return true
    )

  isCheapestMidTerm: Ember.computed('model.adjMidTermCost', 'model.loanStructure.lowestMidTermCost', ->
    if @get('model.adjMidTermCost') == @get('model.loanStructure.lowestMidTermCost')
      return true
    )

  isCheapestTotal: Ember.computed('model.adjLifeTimeCost', 'model.loanStructure.lowestLifeTimeCost', ->
    if @get('model.adjLifeTimeCost') == @get('model.loanStructure.lowestLifeTimeCost')
      return true
    )

  actions:
    removeOffer: ->  
      model = @get('model.content') || @get('model')
      @attrs.removeOffer(model)
      return false


)

`export default PricingSegmentsSingleOfferComparisonComponent`
