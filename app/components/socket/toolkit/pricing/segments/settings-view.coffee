`import Ember from 'ember'`

PricingSegmentsSettingsViewComponent = Ember.Component.extend(

  classNames: ['s24 flex-column-nowrap flex-justify-start flex-align-centre']
  
)

`export default PricingSegmentsSettingsViewComponent`
