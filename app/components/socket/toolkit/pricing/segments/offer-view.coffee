`import Ember from 'ember'`

PricingSegmentsOfferViewComponent = Ember.Component.extend(

  classNames: ['s24 flex-column-nowrap pad5 pricing-offer-view']

  rateSorting: ['liabilityType.sortOrder', 'fixedRateMonths']
  sortedLiabilities: Ember.computed.sort('model.liabilities', 'rateSorting')

  defaultPricingComparisonYrs: Ember.computed('model.defaultPricingComparisonMonths', ->
    return parseInt(@get('model.defaultPricingComparisonMonths')) / 12
    )

)
`export default PricingSegmentsOfferViewComponent`
