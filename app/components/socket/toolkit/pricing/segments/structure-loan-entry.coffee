`import Ember from 'ember'`

LiabilitiesLiabilityTableRowComponent = Ember.Component.extend(

  classNames: ['s24 l18 flex-row-wrap flex-align-centre']

  actions:
    deleteLiability: ->
      model = @get('model.content') || @get('model')
      @attrs.deleteLiability(model)
      ###
      model = @get('model.content') || @get('model')
      loanStructure = model.get('loanStructure.content') || model.get('loanStructure')
      loanStructure.removeLoan(model)
      ###
      return false
)

`export default LiabilitiesLiabilityTableRowComponent`
