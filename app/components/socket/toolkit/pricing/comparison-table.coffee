`import Ember from 'ember'`

PricingComparisonTableComponent = Ember.Component.extend(


  classNames: ['s24 flex-column-nowrap']

  pmtFreq: Ember.computed.alias('model.frequency.name')
  noResults: Ember.computed.equal('model.comparisons.length', 0)

  actions:
    setSortAttribute: (attr) ->
      if attr == "repayments" && @get('model.sortAttribute') == attr
        @changeFrequency()
      else
        @set('model.sortAttribute', attr)
      return false


  changeFrequency: ->
    frequencies = ["Weekly", "Fortnightly", "Monthly"]
    currentFrequency = @get('model.frequency.name')
    nextFrequencyIndex = frequencies.indexOf(currentFrequency) + 1
    if nextFrequencyIndex >= 3
      nextFrequencyIndex = 0
    newFrequency = @store.peekAll('frequency').findBy('name', frequencies[nextFrequencyIndex])
    @set('model.frequency', newFrequency)
    return false

)

`export default PricingComparisonTableComponent`
