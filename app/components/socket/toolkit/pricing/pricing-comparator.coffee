`import Ember from 'ember'`

PricingPricingComparatorComponent = Ember.Component.extend(

  notify: Ember.inject.service('notify')
  session: Ember.inject.service('session')

  classNames: ['s24 pad5 whitesmoke-bg pricing-comparator-body flex-column-nowrap']

  offerSavedObs: Ember.observer('offerSaved', ->
    Ember.run.later(this, ->
      @set 'pricingOffers', @get('model.sortedOffers')
    , 200)
    )


  showInstructions:false
  showPricing: false



  actions:
    addLoan: ->
      model = @get('model.content') || @get('model')
      newLoan = model.addLoan()
      if model.get('liabilities.length') == 1
        model.addPricingOffer()
        model.addPricingOffer()
      newLoan.set('inEditMode', true)


    addOffer: ->
      model = @get('model.content') || @get('model')
      newOffer = model.addPricingOffer()
      newOffer.set('showDetails', true)
      newOffer.set('isInEditMode', true)
      @toggleProperty 'offerSaved' #triggers a refresh of the listing


    deleteLiability: (liability) ->
      model = @get('model.content') || @get('model')
      model.removeLiability(liability)
      ###

      liability.get('pricingOfferRates').forEach (rate) ->
        rate = rate.get('content') || rate
        rate.get('pricingOffer').get('pricingOfferRates').removeObject(rate)
        rate.deleteRecord()
      liability.deleteRecord()
      model = @get('model.content') || @get('model')
      model.get('liabilities').removeObject(liability)
      ###


    deleteOffer: (offer) ->
      model = @get('model.content') || @get('model')
      model.removePricingOffer(offer)
      return false

    toggleSettings:->
      @toggleProperty 'showSettings'
      return false

    toggleOffers: ->
      @toggleProperty 'showPricing'
      return false



  init: ->
    unless @get('model')
      newStructure = @store.createRecord('loanStructure')
      @set('model', newStructure)
    @_super()
    unless @get('session.savedTool')
      @set('showInstructions', true)
)

`export default PricingPricingComparatorComponent`
