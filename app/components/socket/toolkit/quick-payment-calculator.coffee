`import Ember from 'ember'`
`import LoanCalculationsMixin from '../../../mixins/shared/loan-calculations-mixin'`

ToolsToolkitQuickPaymentCalculatorComponent = Ember.Component.extend(LoanCalculationsMixin,

  classNames: ['presentation-object-inner s24 flex-column-nowrap']

  pmtIsWeekly: Ember.computed.equal('model.frequency.name', 'Weekly')
  pmtIsFortnightly: Ember.computed.equal('model.frequency.name', 'Fortnightly')
  pmtIsMonthly: Ember.computed.equal('model.frequency.name', 'Monthly')
  onlyLoanInStructure: false





  lifeTimeCost: Ember.computed('model.remainingMonths', 'model.owing', 'model.interestRate', 'model.interestOnly', ->
    model = @get('model')
    term = model.get('remainingMonths')
    return model.calculateCostToRepayForPeriod(model.get('owing'), term, model.get('interestRate'), 12, model.get('interestOnly'), 0, model.get('interestRate'), term)
  )


  actions:
    close: ->
      @set 'closeAttribute', false

    selectPaymentFreq: (freq) ->
      frequency = @store.peekAll('frequency').findBy('name', freq)
      @set('model.frequency', frequency)



  init: ->
    @_super()
    #@set 'interestType', @get('interestTypes.firstObject')
    unless @get('model')
      newLoan = @store.createRecord('liability',
        owing: 500000
        interestRate: 0.0500
        remainingMonths: 360
      )
      @set 'model', newLoan


)

`export default ToolsToolkitQuickPaymentCalculatorComponent`
