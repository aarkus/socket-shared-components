`import Ember from 'ember'`

ToolkitBudgetBudgetFamilyCardComponent = Ember.Component.extend(

  classNames: ['budget-family-card']

  thisSelected: Ember.computed('model', 'selected', ->
    if @get('model') == @get('selected')
      return true
    )


  actions:
    select: ->
      model = @get('model')
      @attrs.selectType(model)
      return false
)

`export default ToolkitBudgetBudgetFamilyCardComponent`
