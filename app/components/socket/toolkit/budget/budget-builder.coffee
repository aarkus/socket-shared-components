`import Ember from 'ember'`

BudgetBuilderComponent = Ember.Component.extend(

  classNames: ['s24 flex-row-wrap flex-justify-centre flex-align-start whitesmoke-bg pricing-comparator-body']

  session: Ember.inject.service('session')

  umiPositive: Ember.computed.gte('model.umi', 0)

  outcomeColour: Ember.computed('umiPositive', ->
    if @get('umiPositive')
      return 'accent-darker-bg'
    else
      return 'red-bg'
    )

  actions:
    addIncome: (item, type) ->
      newIncome = @get('model').addIncomeOfType(type)
      newIncome.set('isInEditMode', true)
      return false

    addExpense: (item, type) ->
      newExpense = @get('model').addExpenseOfType(type)
      newExpense.set('isInEditMode', true)
      return false

    deleteIncome: (income) ->
      @get('model.incomes').removeObject(income)
      income.deleteRecord()
      return false

    deleteExpense: (expense) ->
      @get('model.expenses').removeObject(expense)
      expense.deleteRecord()
      return false

    newLendingPower: ->
      tool = @store.createRecord('tool',
        name: 'My lending Power'
        toolType: 'Lending Power'
      )

      newLendingPower = @util_CreateLendingPowerFromBudget()

      model = {
        lendingPower: newLendingPower
        tool: tool
      }
      route = @get('router.currentRouteName')
      if route.indexOf('toolkit') == -1
        destinationRoute = "beta.lending-power"
      else
        destinationRoute = "toolkit.lending-power"
      @set('session.savedTool', false)
      @get('router').transitionTo(destinationRoute, model)


    useNZStats: ->
      @set 'showNZStatsEstimate', true
      return false


  init: ->
    @_super()
    lendingPower = @get('session.tools').findBy('name', "Lending Power")
    @set 'lendingPowerTool', lendingPower
    unless @get('session.savedTool')
      @set('showInstructions', true)




  util_CreateLendingPowerFromBudget: ->
    self = @
    newLendingPower = @store.createRecord('lendingPower')

    newLendingPower.set('totalIncome', @get('model.totalIncome'))

    debtServicingExpenses = @get('model.expenses').filterBy('expenseType.name', 'Debt servicing')
    debtServicingValues = debtServicingExpenses.mapBy('monthly')
    totalDebtServicingValue = debtServicingValues.compact().reduce ((a, b) ->
      (parseFloat(a || 0) + parseFloat(b || 0)).toFixed(2)
      ), 0

    nonDebtServicingExpenses = @get('model.expenses').reject (expense) ->
      if debtServicingExpenses.includes(expense)
        return true
    nonDebtServicingValues = nonDebtServicingExpenses.mapBy('monthly')
    totalNonDebtServicingValue = nonDebtServicingValues.compact().reduce ((a, b) ->
      (parseFloat(a || 0) + parseFloat(b || 0)).toFixed(2)
      ), 0


    newLendingPower.set('totalExpenses', totalNonDebtServicingValue)
    newLendingPower.set('totalDebtServicing', totalDebtServicingValue)

    return newLendingPower
)

`export default BudgetBuilderComponent`
