`import Ember from 'ember'`

ToolkitBudgetNzstatsEstimateComponent = Ember.Component.extend(

  classNames: ['s24 presentation-object-inner']
  session: Ember.inject.service('session')
  notify: Ember.inject.service('notify')

  incomeMultipliers: [
    {
      income: "Under $22,800"
      multiplier: 0.429
    },
    {
      income: "$22,801 - $32,100"
      multiplier: 0.489
    },
    {
      income: "$32,101 - $42,500"
      multiplier: 0.614
    },
    {
      income: "$42,501 - $55,500"
      multiplier: 0.723
    },
    {
      income: "$55,501 - $68,600"
      multiplier: 0.854
    },
    {
      income: "$68,601 - $82,500"
      multiplier: 0.994
    },
    {
      income: "$82,501 - $100,100"
      multiplier: 1.11
    },
    {
      income: "$100,101 - $123,900"
      multiplier: 1.25
    },
    {
      income: "$123,901 - $165,900"
      multiplier: 1.50
    },
    {
      income: "Over $165,901"
      multiplier: 2.04
    }
  ]

  householdTypes: [
    {
      title: "Single, No Children"
      spend: 2346.94
      iconPath: 'icons/budget/family-single'
    },
    {
      title: "Couple, No Children"
      spend: 4513.45
      iconPath: 'icons/budget/family-couple'

    },
    {
      title: "Single, 1+ Children"
      spend: 2581.37
      iconPath: 'icons/budget/family-single-1-child'
    },
    {
      title: "Couple, 1 Child"
      spend: 4703.05
      iconPath: 'icons/budget/family-couple-1-child'
    },
    {
      title: "Couple, 2 Children"
      spend: 5321.93
      iconPath: 'icons/budget/family-couple-2-children'
    },
    {
      title: "Couple, 3+ Children"
      spend: 5538.35
      iconPath: 'icons/budget/family-couple-3-children'
    }


  ]

  estimatedExpenditure: Ember.computed('household', 'income', ->
    unless @get('household') and @get('income')
      return 0
    baseExp = @get('household.spend')
    multiplier = @get('income.multiplier')
    return baseExp * multiplier
    )



  actions:
    addExpense: ->
      budget = @get('model')
      notify = @get('notify')
      estimatedExpenditure = @get('estimatedExpenditure')
      houseHoldExpense = @store.peekAll('expenseType').findBy('name', 'Household')
      newExpense = budget.addExpenseOfType(houseHoldExpense)
      newExpense.set('value', estimatedExpenditure)
      newExpense.set('isSocketEstimate', true)
      newExpense.set('description', "Estimated Monthly Expenses")
      notify.success('Expense estimate added')
      @set 'closeAttribute', false
      return false


    setHousehold: (household) ->
      @set 'household', household
      return false

    setIncome: (income) ->
      @set 'income', income
      return false

)

`export default ToolkitBudgetNzstatsEstimateComponent`
