`import Ember from 'ember'`

ToolkitBudgetBudgetFamilyCardComponent = Ember.Component.extend(

  classNames: ['budget-family-income-segment']

  classNameBindings: [ 'selected', ':budget-family-income-segment' ]

  selected: Ember.computed('model', 'selectedIncome', ->
    if @get('model') == @get('selectedIncome')
      return true
    )


  actions:
    select: ->
      model = @get('model')
      @attrs.selectType(model)
      return false
)

`export default ToolkitBudgetBudgetFamilyCardComponent`
