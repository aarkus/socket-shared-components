`import Ember from 'ember'`
`import accounting from 'accounting'`

ToolsLoanPlannerComponent = Ember.Component.extend(
  notify: Ember.inject.service('notify')
  session: Ember.inject.service('session')

  classNames: ['s24 pad5 flex-row-wrap whitesmoke-bg']

  showInstructions: false

  showLoanPlanEventType: false
  showSelectedEvent: false
  showProposition: false
  showYear: false
  showInstructionInset: true

  dragData: {}

  hasSavings: Ember.computed('yearsSaved', 'totalSavings', ->
    return true if @get('yearsSaved') > 0 or @get('totalSavings') > 0
    )
  hasNegativeSavings: Ember.computed('hasSavings', 'yearsSaved', 'totalSavings', ->
    return false if @get('hasSavings')
    return true if @get('yearsSaved') < 0 or @get('totalSavings') < 0
    )

  longestUserDefinedTerm: Ember.computed.max('model.terms') #used by obsTerms to inform user of term discrepancies on graphs

  totalSavings: Ember.computed('model.totalInterestBaseline',  'model.totalPrincipalBaseline', 'model.totalInterestPlan',  'model.totalPrincipalPlan', ->
    savings = @get('model.planInterestSavings')
    return savings
    )

  yearsSaved: Ember.computed('model.runningBalancesPlan',  'model.runningBalancesBaseline', ->
    if @get('model.runningBalancesPlan') && @get('model.runningBalancesBaseline')
      baselineYrs = @get('model.runningBalancesBaseline').without(0).compact().length
      planYrs = @get('model.runningBalancesPlan').without(0).compact().length
      return baselineYrs - planYrs
    )

  hideDragHereOverlay: Ember.computed('model.events.[]', ->
    if @get('model.events.length') > 0
      return true
    )


  interestRate: Ember.computed('model.interestRate', ->
    interestRate = (@get('model.interestRate') * 100).toFixed(2)
    return "#{interestRate}% p.a"
    )

  singeLoanOnly: Ember.computed.equal('model.loans.length', 1)

  unreadUserMessages: Ember.computed.filterBy('model.userMessages', 'isRead', false)


  obsTerms: Ember.observer('showProposition', ->
    longestModelTerm = @get('longestUserDefinedTerm')
    baselineLength = @get('model.runningBalancesBaseline').without(0).length

    unless Math.ceil(longestModelTerm/12) == baselineLength
      unless @get 'multiTermNotified'
        notify = @get('notify')
        message= "<span>You'll notice the term of our graphs may not be as long as the longest term loan in your plan.<br/> We've assumed that once the shorter term loans are repaid, you'll increase the repayments on the longer term loans to keep your outgoings the same, and repay the loans faster. <br/> To extend the term of your plan, simply drop in a 'recalculate payments' event<span>"
        notify.info({html: message, closeAfter: null})
        @set 'multiTermNotified', 'multiTermMessage'
    )

  sortedEvents: Ember.computed('model.events.@each.year', ->
    return @get('model.events').sortBy('year')
    )

  term: Ember.computed('model.term', ->
    return @get('model.term')/12 + ' yrs'
    )


  modelObs: Ember.observer('model', -> #this to ensure when opening a new model from same route, everything goes smoothly
    @refreshData()
    Ember.run.later(this, ->
      @set 'showProposition', true
      @refreshData() #to ensure icons go to the right place
    ,1200)
    )

  init: ->
    @_super()
    if @store.peekAll('loan-plan-event-type').get('length') == 0
      @createEventTypes()
    @set 'eventTypes', @store.peekAll('loan-plan-event-type')
    ###
    unless @get('session.savedTool')
      @set('showInstructions', true)
      ###
    Ember.run.later(this, ->
      @set 'showProposition', true
      @refreshData() #to ensure icons go to the right place
    ,1200)



  actions:
    addLoan: ->
      plan = @get('model')
      plan.addLoanPortion()

    closeProposition: ->
      @set 'showProposition', false
      return false

    displayEventDescription: (eventType) ->
      @clearSelections()
      @set 'selectedEventType', eventType
      if @get('reduceOverlay')
        @set 'reduceOverlay', false
        Ember.run.later(this, 'toggleEventDetails', 400)
      else
        @toggleEventDetails()

    displaySelectedEvent: (event) ->
      @clearSelections()
      @set 'selectedEvent', event
      if @get('reduceOverlay')
        @set 'reduceOverlay', false
        Ember.run.later(this, 'toggleEvent', 400)
      else
        @toggleEvent()

    displaySelectedYear: (year) ->
      @clearSelections()
      @set 'selectedYear', year
      if @get('reduceOverlay')
        @set 'reduceOverlay', false
        Ember.run.later(this, 'toggleYear', 400)
      else
        @toggleYear()

    displayInstructions: ->
      unless @get('reduceOverlay')
        @clearSelections()
      @toggleProperty('reduceOverlay')
      Ember.run.later(this, 'toggleInstructions', 400)
      return false

    hideInstructions: ->
      @clearSelections()
      @set 'reduceOverlay', true
      Ember.run.later(this, 'toggleInstructionIcon', 400)

    showUserMessages: ->
      notify = @get('notify')
      @get('unreadUserMessages').forEach (msg) ->
        message = "<span>#{msg.get('message')}<span>"
        notify.info({html: message, closeAfter: null})

    toggleShowProposition: ->
      @toggleProperty 'showProposition'
      return false




  clearSelections: ->
    self = @
    ['showInstructionInset', 'showInstructionIcon', 'showLoanPlanEventType', 'showSelectedEvent', 'showYear', 'loanPlanEventType', 'selectedYear', 'selectedEvent' ].forEach (show) ->
      self.set(show, false)

  refreshData: ->
    model = @get('model')
    model.recalculatePlan()

  toggleInstructions: ->
    @set('showInstructionInset', true)
    return false

  toggleEventDetails: ->
    @set 'showLoanPlanEventType', true
    return false

  toggleYear: ->
    unless @get('showYear')
      @set 'showYear', true
    return false

  toggleEvent:->
    @set 'showSelectedEvent', true
    return false

  toggleInstructionIcon: ->
    @set 'showInstructionIcon', true
    return false

  createEventTypes: ->
    if @store.peekAll('loanPlanEventType').get('length') == 0
      self = @
      eventTypes = [
        {
          name: 'Lump sum payment'
          eventOrder: 3
          iconPath: 'icons/loan-event-lump-sum'
          description: "Make an additional contribution to your loan over and above your loan payments"
          notes: [
            'Some banks have restrictions on the number or value of lump sum payments you can make to certain loan types. Consult your adviser'
          ]
        }, {
          name: 'Borrow more'
          eventOrder: 2
          iconPath: 'icons/loan-event-borrow-more'
          description: "Borrow more money - perhaps to purchase an investment property or a Ferrari!"
          notes: [
            "New lending criteria will need to be met, although it may not always require a full application - consult your adviser"
          ]
        }, {
          name: 'Round up'
          iconPath: 'icons/loan-event-round-up'
          eventOrder: 9
          description: "Round your payments up to the nearest round figure - you'll be amazed how much you can save!"
          notes: [
            "All payments after this event will be rounded up, including if you increase or reduce payments"
          ]
        }, {
          name: 'Increase payments'
          iconPath: 'icons/loan-event-increase-payments'
          eventOrder: 4
          description: "Increase your payments by a dollar figure or percentage to pay your loan off quicker"
          notes: [
            "Higher payments are a great way to pay your loan off faster - a good idea if your likely to need to reduce payments/take a payment holiday later"
          ]
        }, {
          name: 'Reduce payments'
          iconPath: 'icons/loan-event-reduce-payments'
          eventOrder: 5
          description: "Reduce your payments - a good option when affordability might be strained"
          notes: [
            "Lenders may only allow you to reduce your payments if you are ahead on your loan. Consult your adviser"
          ]
        }, {
          name: 'Recalculate payments'
          iconPath: 'icons/loan-event-repayment-holiday'
          eventOrder: 6
          description: "Adjust your payments to be paid off by a certain year"
          notes: [
            "Choose a year you'd like to be paid off by and we'll adjust your payments to suit - rounding and recurring payment adjustments will still apply"
          ]
        }, {
          name: 'Interest only'
          iconPath: 'icons/loan-event-interest-only'
          eventOrder: 8
          description: "Reduce your payments to only cover the interest for a fixed period of time"
          notes: [
            "Lenders have strict criteria regarding interest-only loans, including maximum terms and qualification criteria. Consult your adviser"
          ]
        }, {
          name: 'Rate change'
          iconPath: 'icons/loan-event-rate-change'
          eventOrder: 7
          description: "Interest rates change like the weather. Include a provision for a higher (or lower) interest rate"
          notes: [
            "A higher interest rate may require higher payments - consider raising your payments to cover a higher interest rate"
          ]
        }, {
          name: 'Pay off loan'
          iconPath: 'icons/loan-event-pay-off'
          eventOrder: 10
          description: "Leave this life of debt!  Pay your home loan off with an inheritance, property sale, or sell the Ferrari (don't!)"
          notes: [
            "Selecting payoff will terminate the loan at this point"
          ]
        }
      ]
      eventTypes.forEach (type) ->
        self.store.createRecord('loanPlanEventType',
          name: type.name
          iconPath: type.iconPath
          eventOrder: type.eventOrder
          description: type.description
          notes: type.notes

        )





)

`export default ToolsLoanPlannerComponent`
