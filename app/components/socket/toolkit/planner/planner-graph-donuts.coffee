`import Ember from 'ember'`
`import accounting from 'accounting'`

ToolsPlannerPlannerGraphDonutsComponent = Ember.Component.extend(

  classNames: ['s24 flex-row-wrap']


  totalCostBaseline: Ember.computed('model.totalInterestBaseline', 'model.totalPrincipalBaseline', ->
    return accounting.formatMoney(@get('model.totalInterestBaseline') + @get('model.totalPrincipalBaseline'), '$', 2)
    )

  totalCostPlan: Ember.computed('model.totalInterestPlan', 'model.totalPrincipalPlan', ->
    return accounting.formatMoney(@get('model.totalInterestPlan') + @get('model.totalPrincipalPlan'), '$', 2)
    )



  dataObserver: Ember.observer('model.totalInterestBaseline',  'model.totalPrincipalBaseline', 'model.totalInterestPlan',  'model.totalPrincipalPlan', ->
    #@toggleProperty 'dataUpdate'
    Ember.run.once(this, 'updateBaselineDonut')
    Ember.run.once(this, 'updatePlanDonut')
  )

  donutOptions:
    responsive: true
    maintainAspectRatio: true
    legend:
      display: false
    hover:
      mode: 'label'
    tooltips:
      mode: 'label'
      callbacks:
        title: (tooltipItem, data) ->
          if data.datasets[0].backgroundColor[1] == "rgba(157, 207, 242, 1.00)"
            return 'Baseline'
          else
            return 'Plan'
        label: (tooltipItem, data) ->
          label = "#{data.labels[tooltipItem.index]}: #{accounting.formatMoney(data.datasets[0].data[tooltipItem.index], '$', 2)}"
          return label




  init: ->
    @_super()
    unless @get('lineChart')
      Ember.run.later(this, ->
        @initBaselineDonut()
        @initPlanDonut()
        1200
        )




  initBaselineDonut: ->
    donutOptions = @get 'donutOptions'
    baselineDonutChartData = {
      labels: ['Interest', 'Principal']
      datasets: [
        {
          data: [1, 1]
          backgroundColor: ['rgba(116, 118, 120, 1.00)', 'rgba(157, 207, 242, 1.00)']
          hoverBackgroundColor: ['rgba(116, 118, 120, 0.8)', 'rgba(157, 207, 242, 0.8)']
        }
      ]
    }

    ctx = document.getElementById("baselineDonutChart").getContext("2d");

    baselineDonutChart = new Chart(ctx,
      type: 'doughnut'
      data: baselineDonutChartData
      options: donutOptions
    )
    @set 'baselineDonut', baselineDonutChart


  initPlanDonut: ->
    donutOptions = @get 'donutOptions'
    planDonutChartData = {
      labels: ['Interest', 'Principal']
      datasets: [
        {
          data: [1, 1]
          backgroundColor: ['rgba(116, 118, 120, 1)', 'rgba(72, 160, 220, 1)']
          hoverBackgroundColor: ['rgba(116, 118, 120, 0.8)', 'rgba(72, 160, 220, 0.8)']
        }
      ]
    }
    if @get('print')
      ctx = document.getElementById("planDonutChartPrint").getContext("2d");
    else
      ctx = document.getElementById("planDonutChart").getContext("2d");

    planDonutChart = new Chart(ctx,
      type: 'doughnut'
      options: donutOptions
      data: planDonutChartData
    )
    @set 'planDonut', planDonutChart
    @updatePlanDonut()


  updateBaselineDonut: ->
    principalBaseline = @get('model.totalPrincipalBaseline')
    totalInterestBaseline = @get('model.totalInterestBaseline')
    baselineDonut = @get 'baselineDonut'
    #labels = []

    #lineChart.data.labels = labels
    if baselineDonut
      baselineDonut.data.datasets[0].data = [totalInterestBaseline, principalBaseline]
      #lineChart.data.datasets[1].data = runningBalancesPlan
      baselineDonut.update()

  updatePlanDonut: ->
    principalPlan = @get('model.totalPrincipalPlan')
    totalInterestPlan = @get('model.totalInterestPlan')
    planDonut = @get 'planDonut'
    #labels = []

    #lineChart.data.labels = labels
    planDonut.data.datasets[0].data = [totalInterestPlan, principalPlan]
    #lineChart.data.datasets[1].data = runningBalancesPlan
    planDonut.update()
)

`export default ToolsPlannerPlannerGraphDonutsComponent`
