`import Ember from 'ember'`

ToolsPlannerPlannerDraggableEventIconComponent = Ember.Component.extend(

  classNames: ['selectable relative']
  attributeBindings: [ 'draggable' ]
  draggable: 'true'

  yearOver: null

  dragStart: (e) ->
    e.dataTransfer.setData("text", e.target.id);
    @pickupIcon(e)
    return e

  drag: (e) ->
    @dragIcon(e)

  dragEnd: (e) ->
    @dropIcon(e)


  touchStart: (e) ->
    @pickupIcon(e)


  touchMove: (e) ->
    e.preventDefault()
    @dragIcon(e)


  touchEnd: (e) ->
    @dropIcon(e)


  pickupIcon: (e) ->
    if e.originalEvent.targetTouches
      clientX = e.originalEvent.targetTouches[0].clientX
      clientY = e.originalEvent.targetTouches[0].clientY
    else
      clientX = e.originalEvent.clientX
      clientY = e.originalEvent.clientY
    @set 'touchOrigin', {x: clientX, y:clientY}



  dragIcon: (e) ->
    if e.originalEvent.touches
      clientX = e.originalEvent.touches[0].clientX
      clientY = e.originalEvent.touches[0].clientY
    else
      clientX = e.originalEvent.clientX
      clientY = e.originalEvent.clientY
    if clientX == 0 and clientY == 0
      return false
    xChange = clientX - @get('touchOrigin').x
    yChange = clientY - @get('touchOrigin').y
    changeString = "translate(#{xChange}px,#{yChange}px)"
    @utilTransformIconCss(changeString)
    @set 'hideDragHereOverlay', true
    yearAtEvent = @utilGetYearAtEvent(e)
    maxYears = @get('maxYears')
    if yearAtEvent > 0 && yearAtEvent <= maxYears + 1
      @set 'yearOver', yearAtEvent
    #else
      #@set 'yearOver', null

  dropIcon: (e) ->
    self = @
    changeString = "translate(0px,0px) scale(1)"
    @utilTransformIconCss(changeString)
    yearAtEvent = @get('yearOver')
    maxYears = @get('maxYears')
    if yearAtEvent > 0 && yearAtEvent <= maxYears
      model = @get('model')
      plan = @get('plan')
      newEvent = plan.addEventOfTypeToYear(model, yearAtEvent)
      @attrs.displaySelectedEvent(newEvent)

    @set 'yearOver', null



  utilGetYearAtEvent: (e) ->
    lineChart = @get('lineChart')
    yAxis0 = lineChart.scales['y-axis-0'].right + 23
    xAxisWidth = lineChart.scales['x-axis-0'].width
    yearsOnX = lineChart.scales['x-axis-0'].ticks.length - 1
    pxPerYear = xAxisWidth / yearsOnX

    @set('maxYears', yearsOnX)

    if e.originalEvent.changedTouches
      xLocation = e.originalEvent.changedTouches[0].clientX
    else
      xLocation = e.originalEvent.clientX
    yearAtEvent = Math.round((xLocation - yAxis0) / pxPerYear)
    return yearAtEvent


  utilTransformIconCss: (transform) ->
    thisIconId = @get('elementId')
    $('#' + thisIconId).css('transform':transform)
    $('#' + thisIconId).css('-webkit-transform':transform)
    $('#' + thisIconId).css('-ms-transform':transform)

)

`export default ToolsPlannerPlannerDraggableEventIconComponent`
