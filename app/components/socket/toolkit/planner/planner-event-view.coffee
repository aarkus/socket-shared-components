`import Ember from 'ember'`

ToolsPlannerPlannerEventViewComponent = Ember.Component.extend(

  selectedEventType: null
  valueIsPercent: false
  repeatFrequency: 0


  lumpSum: Ember.computed.equal('model.eventType.name', 'Lump sum payment')
  borrowMore: Ember.computed.equal('model.eventType.name', 'Borrow more')
  roundUp: Ember.computed.equal('model.eventType.name', 'Round up')
  increasePayments: Ember.computed.equal('model.eventType.name', 'Increase payments')
  reducePayments: Ember.computed.equal('model.eventType.name', 'Reduce payments')
  recalculatePayments: Ember.computed.equal('model.eventType.name', 'Recalculate payments')
  intOnly: Ember.computed.equal('model.eventType.name', 'Interest only')
  rateChange: Ember.computed.equal('model.eventType.name', 'Rate change')
  payOff: Ember.computed.equal('model.eventType.name', 'Pay off loan')

  modelObs: Ember.observer('model', -> #clears the form when the model changes
    @utilClearForm()
    @utilSetupForm()
    )

  eventPercentObs: Ember.observer('eventPercent', 'percentRecurrence', ->
    eventValue = @get('eventPercent')
    if eventValue > 0
      percentRecurrence = @get('percentRecurrence') || 0
      @set 'eventValue', null
      @set 'dollarRecurrence', 0
      @set 'valueIsPercent', true
      @set 'model.eventValue', eventValue
      @set 'model.eventValueType', 'percent'
      @set 'model.recurrenceInterval', percentRecurrence
    )

  eventValueObs: Ember.observer('eventValue', 'dollarRecurrence', ->
    eventValue = @get('eventValue')
    if eventValue > 0
      @set 'eventPercent', null
      @set 'percentRecurrence', 0
      @set 'valueIsPercent', false
      @set 'model.eventValue', eventValue
      @set 'model.eventValueType', 'value'
      @set 'model.recurrenceInterval', @get('dollarRecurrence')
    )

  actions:
    destroyEvent: ->
      @get('model.loanPlan.events').removeObject(@model)
      @model.deleteRecord()
      @attrs.closePopover()
      return false

    saveEvent: ->
      @attrs.closePopover()
      return false

    toggleValueIsPercent: ->
      @toggleProperty('valueIsPercent')
      if @get('valueIsPercent')
        @set 'model.eventValueType', 'percent'
      else
        @set 'model.eventValueType', 'value'
      return false

  init: ->
    @_super()
    @utilClearForm()
    @utilSetupForm()


  utilClearForm: ->
    @setProperties(
      eventPercent: null
      eventValue: null
      percentRecurrence: null
      dollarRecurrence: null
      valueIsPercent: false
    )

  utilSetupForm: ->
    if @get('model.eventValueType') == 'percent'
      @set 'eventPercent', @get('model.eventValue')
      @set 'percentRecurrence', @get('model.recurrenceInterval')
      @set 'valueIsPercent', true
    else
      @set 'eventValue', @get('model.eventValue')
      @set 'dollarRecurrence', @get('model.recurrenceInterval')



)

`export default ToolsPlannerPlannerEventViewComponent`
