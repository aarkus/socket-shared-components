`import Ember from 'ember'`

ToolsPlannerPlannerMyLoanEventsTableRowComponent = Ember.Component.extend(

  classNames: ['s24 element-table-row']


  isPercent: Ember.computed.equal('model.eventValueType', 'percent')
  isYear: Ember.computed.equal('model.eventType.name', 'Recalculate payments')
  isNotCurrency: Ember.computed.or('isPercent', 'isYear')
  isCurrency: Ember.computed.not('isNotCurrency')

  eventValue: Ember.computed('model.eventValue', 'isYear', ->
    unless @get('isYear')
      return @get('model.eventValue')
    else
      return "Pay off loan by Year #{@get('model.eventValue')}"
    )

  actions:
    viewEvent: (event) ->
      @set 'selectedEvent', event
      @set 'viewEvent', true
      return false

)

`export default ToolsPlannerPlannerMyLoanEventsTableRowComponent`
