`import Ember from 'ember'`

ToolsPlannerPlannerDraggableEventComponent = Ember.Component.extend(

  #'flex-column-nowrap', 'flex-align-centre', 'flex-equal-size']
  classNames: ['planner-event']

  click: ->
    eventType = @get('model')
    @attrs.displayEventDescription(eventType)
    return false
)

`export default ToolsPlannerPlannerDraggableEventComponent`
