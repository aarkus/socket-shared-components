`import Ember from 'ember'`
`import accounting from 'accounting'`

ToolsPlannerPlannerGraphLineComponent = Ember.Component.extend(

  classNames: ['s24']

  classNameBindings: [ 'dragClass', ':relative' ]
  dragClass: 'deactivated'

  dragLeave: (e) ->
    e.preventDefault()

  dragOver: (e) ->
    e.preventDefault()

  drop: (e) ->
    e.preventDefault()



  click: (e) ->
    lineChart = @get('lineChart')
    jQueryEvent = $.Event('click')
    jQueryEvent.currentTarget = e.target
    jQueryEvent.originalEvent = e.originalEvent
    chartElements = lineChart.getElementsAtEvent(jQueryEvent)
    if chartElements.length > 0
      index = chartElements[0]._index
      @attrs.displaySelectedYear(index)



  init: ->
    @_super()
    self = @
    @set 'plannerComponent', this
    Ember.run.later(this, ->
      @initBaselineGraph()
      1000
      )

  
  dataObserver: Ember.observer('model.runningBalancesPlan',  'model.runningBalancesBaseline',  ->
    Ember.run.once(this, 'updateLineGraph')
  )


  initBaselineGraph: ->
    labels = []
    data = []

    i = 0
    while i < 50
      labels.pushObject(i)
      data.pushObject(0)
      i++

    lineChartData = {
      labels: labels
      datasets: [
        {
          lineTension: 0.15
          label: 'plan'
          data: data #runningBalancesBaseline
          borderColor: 'rgba(72, 160, 220, 1.00)'
          backgroundColor: 'rgba(72, 160, 220, 0.3)'
        }, {
          lineTension: 0.15
          label: 'baseline'
          data: data #runningBalancesBaseline
          #borderColor: 'rgba(116, 118, 120, 1.00)'
          #backgroundColor: 'rgba(116, 118, 120, 0.1)'
          borderColor: 'rgba(157, 207, 242, 1.00)'
          backgroundColor: 'rgba(157, 207, 242, 0.2)'
        }
      ]
    }
    if @get('print')
      ctx = document.getElementById("lineChartPrint").getContext("2d");
    else
      ctx = document.getElementById("lineChart").getContext("2d");

    lineChart = new Chart(ctx,
      type: 'line'
      data: lineChartData
      options:
        responsive: true
        maintainAspectRatio: false
        events:
          [
            #'dragover'
            'mousemove'
            'mouseout'
            'click'
            'touchstart'
            'touchmove'
            'touchend'
          ]
        elements:
          point: {hitRadius: 10}
        scales: {
          yAxes: [
            scaleLabel:
              display: true
              labelString: 'Loan Balance'
            beginAtZero: true
            ticks: {
              callback: (value, index, values) ->
                  return accounting.formatMoney(value, '$', 0)
              }
          ]
        }
        legend: {display: false}
        hover: {mode: 'single'}
        tooltips:
          mode: 'label'
          callbacks:
            title: (tooltipItem, data) ->
              return 'Year ' + tooltipItem[0].index
            label: (tooltipItem, data) ->
              if tooltipItem.datasetIndex == 0
                type = 'Plan: '
              else
                type = 'Baseline: '
              label = tooltipItem.yLabel || 0
              return type +  accounting.formatMoney(label, '$', 2)
    )
    @set 'lineChart', lineChart
    @get('model').recalculatePlan()



  updateLineGraph: ->
    runningBalancesBaseline = @get('model.runningBalancesBaseline')
    runningBalancesPlan = @get('model.runningBalancesPlan')
    lineChart = @get 'lineChart'
    labels = []
    i = 0
    if runningBalancesPlan
      longestLength = Math.max(runningBalancesBaseline.length, runningBalancesPlan.length)
    else
      longestLength = runningBalancesBaseline.length
      runningBalancesPlan = runningBalancesBaseline

    while i < longestLength
      labels.pushObject(i)
      i++
    lineChart.data.labels = labels
    lineChart.data.datasets[1].data = runningBalancesBaseline
    lineChart.data.datasets[0].data = runningBalancesPlan
    lineChart.update()


)

`export default ToolsPlannerPlannerGraphLineComponent`
