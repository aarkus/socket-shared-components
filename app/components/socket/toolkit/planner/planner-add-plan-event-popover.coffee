`import Ember from 'ember'`

ToolsPlannerPlannerAddPlanEventPopoverComponent = Ember.Component.extend(

  classNames: ['tools-popover-inner-container']

  selectedEventType: null
  valueIsPercent: false

  lumpSum: Ember.computed.equal('model.eventType', 'Lump sum payment')
  borrowMore: Ember.computed.equal('model.eventType', 'Borrow more')
  roundUp: Ember.computed.equal('model.eventType', 'Round up')
  increasePayments: Ember.computed.equal('model.eventType', 'Increase payments')
  reducePayments: Ember.computed.equal('model.eventType', 'Reduce payments')
  pmtHoliday: Ember.computed.equal('model.eventType', 'Repayment holiday')
  intOnly: Ember.computed.equal('model.eventType', 'Interest only')
  rateChange: Ember.computed.equal('model.eventType', 'Rate change')
  payOff: Ember.computed.equal('model.eventType', 'Pay off loan')


  actions:
    addEvent: (type) ->
      @set 'selectedEventType', type
      eventType = @get 'selectedEventType'
      @set 'model.eventType', eventType.name
      @set 'model.eventOrder', eventType.eventOrder
      @set 'model.iconPath', eventType.iconPath
      $('#eventDetails').slideDown()
      $('#eventSelect').slideUp()

    addSelectedEvent: ->
      @set 'closeAttribute', false


    toggleValueIsPercent: ->
      @toggleProperty 'valueIsPercent'
      return false


  init: ->
    @_super()
    unless @get('model')
      plan = @get 'plan'
      year = @get 'year'
      newEvent = plan.addEvent(year)
      @set 'model', newEvent
    eventTypes = @createEventTypes()
    unless @get 'existingEvent'
      @set 'selectedEventType', eventTypes[0]
    @set 'eventTypes', eventTypes



  didInsertElement: ->
    @_super()
    if @get 'existingEvent'
      $('#eventSelect').slideUp(2)
    else
      $('#eventDetails').slideUp(2)


  createEventTypes: ->
    eventTypes = [
      {
        name: 'Lump sum payment'
        eventOrder: 3
        iconPath: 'icons/loan-event-lump-sum'
        description: 'Make an additional contribution to your loan over and above your loan repayments'
        notes: [ 'Some banks have restrictions on the number or value of lump sum payments you can make to certain loan types. Check with your lender or adviser' ]
      }
      {
        name: 'Borrow more'
        eventOrder: 2
        iconPath: 'icons/loan-event-borrow-more'
        description: 'Borrow more money for something awesome - like buying an investment property, a bigger house, or a Ferrari GTC4Lusso!'
        notes: [ 'You will need to meet a Lender\'s criteria to borrow more money, although it may not always require a full loan application - your adviser will be able to assist you here' ]
      }
      {
        name: 'Round up'
        iconPath: 'icons/loan-event-round-up'
        eventOrder: 10
        description: 'Round your payments up to the nearest round figure - you\'ll be amazed how much you can save!'
        notes: [ 'All payments after this event will be rounded up, including if you increase or reduce payments' ]
      }
      {
        name: 'Increase payments'
        iconPath: 'icons/loan-event-increase-payments'
        eventOrder: 4
        description: 'Increase your repayments by a dollar figure or percentage to pay your loan off quicker'
      }
      {
        name: 'Reduce payments'
        iconPath: 'icons/loan-event-reduce-payments'
        eventOrder: 5
        description: 'Reduce your repayments, maybe because you want to stay in debt your whole life, or maybe because your income will be reduced for a period of time'
        notes: [ 'Lenders will typically only allow you to reduce your repayments if you will still repay your home loan in the original timeframe. Consult your adviser' ]
      }
      {
        name: 'Repayment holiday'
        iconPath: 'icons/loan-event-repayment-holiday'
        eventOrder: 6
        description: 'Take a break from those pesky loan repayments for a fixed period of time. Great for life events where your income is impacted temporarily, like having a baby'
        notes: [ 'Loan repayment holidays are not available with all lenders, and strict criteria apply. Consult your adviser' ]
      }
      {
        name: 'Interest only'
        iconPath: 'icons/loan-event-interest-only'
        eventOrder: 7
        description: 'Reduce your repayments to only cover the interest for a fixed period of time'
        notes: [ 'Lenders have strict criteria regarding interest-only loans, including maximum terms and qualification criteria. Consult your adviser' ]
      }
      {
        name: 'Rate change'
        iconPath: 'icons/loan-event-rate-change'
        eventOrder: 8
        description: 'Interest rates change like Auckland\'s weather. Include a provision for a higher (or lower) interest rate'
        notes: [ 'A higher interest rate may require higher repayments - consider raising your repayments to cover a higher interest rate' ]
      }
      {
        name: 'Pay off loan'
        iconPath: 'icons/loan-event-pay-off'
        eventOrder: 9
        description: 'Leave this life of debt!  Pay your home loan off with an inheritance, property sale, or sell the Ferrari (don\'t!)'
        notes: [ 'Selecting payoff will terminate the loan at this point' ]
      }
    ]

)

`export default ToolsPlannerPlannerAddPlanEventPopoverComponent`
