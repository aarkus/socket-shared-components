`import Ember from 'ember'`
`import accounting from 'accounting'`

ToolsPlannerPlannerGraphPaymentComponent = Ember.Component.extend(


  classNames: ['s24']

  click: (e) ->
    pmtChart = @get('pmtChart')
    jQueryEvent = $.Event('click')
    jQueryEvent.currentTarget = e.target
    jQueryEvent.originalEvent = e.originalEvent
    chartElements = pmtChart.getElementsAtEvent(jQueryEvent)
    if chartElements.length > 0
      index = chartElements[0]._index
      @set 'selectedYear', index

  didRender: ->
    @_super()
    unless @get('pmtChart')
      Ember.run.later(this, ->
        @initBaselineGraph()
        2000
        )


  dataObserver: Ember.observer('model.runningPaymentsPlanInterest',  'model.runningPaymentsPlanPrincipal',   ->
    Ember.run.once(this, 'updatePmtGraph')
  )


  initBaselineGraph: ->
    labels = []
    data = []

    pmtChartData = {
      labels: labels
      datasets: [
        {
          label: 'principal'
          data: data
          backgroundColor: 'rgba(72, 160, 220, 1)'
        }, {
          label: 'interest'
          data: data
          backgroundColor: 'rgba(116, 118, 120, 1)'
        }
      ]
    }
    if @get('print')
      ctx = document.getElementById("pmtChartPrint").getContext("2d");
    else
      ctx = document.getElementById("pmtChart").getContext("2d");
    pmtChart = new Chart(ctx,
      type: 'bar'
      data: pmtChartData
      options:
        stacked: true
        responsive: true
        maintainAspectRatio: false
        scales: {
          xAxes: [
            categoryPercentage: 0.5
            stacked: true
          ]
          yAxes: [
            scaleLabel:
              display: true
              labelString: 'Loan Payments'
            beginAtZero: true
            stacked: true
            ticks: {
              callback: (value, index, values) ->
                  return accounting.formatMoney(value, '$', 2)
              }
          ]
        }
        legend: {display: false}
        tooltips:
          mode: 'label'
          callbacks:
            title: (tooltipItem, data) ->
              pmt = tooltipItem[0].yLabel + tooltipItem[1].yLabel
              return 'Year ' + tooltipItem[0].index + ' - ' + accounting.formatMoney(pmt, '$', 2)
            label: (tooltipItem, data) ->
              if tooltipItem.datasetIndex == 0
                type = 'Principal: '
              else
                type = 'Interest: '
              return type +  accounting.formatMoney(tooltipItem.yLabel, '$', 2)


    )
    @set 'pmtChart', pmtChart


  updatePmtGraph: ->
    interestPmts = @get('model.runningPaymentsPlanInterest')
    principalPmts = @get('model.runningPaymentsPlanPrincipal')

    if interestPmts && principalPmts
      pmtChart = @get 'pmtChart'
      labels = []
      i = 0
      while i < principalPmts.length + 1
        labels.pushObject(i)
        i++
      pmtChart.data.labels = labels
      pmtChart.data.datasets[0].data = principalPmts
      pmtChart.data.datasets[1].data = interestPmts
      pmtChart.update()



)

`export default ToolsPlannerPlannerGraphPaymentComponent`
