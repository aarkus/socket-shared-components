`import Ember from 'ember'`
`import TableRowOperationsMixin from 'socket-toolkit/mixins/shared/table-row-operations-mixin'`

ToolsPlannerPlannerStructureTableRowComponent = Ember.Component.extend(TableRowOperationsMixin,


  init: ->
    @_super()
    loanTypes = @store.peekAll('liabilityType').filter (type) ->
      if ['Revolving credit', 'Fixed home loan', 'Floating home loan'].includes(type.get('name'))
        return true
    @set 'loanTypes', loanTypes
)

`export default ToolsPlannerPlannerStructureTableRowComponent`
