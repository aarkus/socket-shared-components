`import Ember from 'ember'`

ToolsPlannerPlannerGraphYearsSavedComponent = Ember.Component.extend(

  classNames: ['s24']

  baselineYears: Ember.computed('model.runningBalancesBaseline.[]', ->
    if @get('model.runningBalancesBaseline')
      @get('model.runningBalancesBaseline').without(0).length
    )

  planYears: Ember.computed('model.runningBalancesPlan.[]', ->
    if @get('model.runningBalancesPlan')
      @get('model.runningBalancesPlan').without(0).length
    )


  didRender: ->
    @_super()
    unless @get('yrsChart')
      Ember.run.later(this, ->
        @initBaselineGraph()
        2000
        )


  dataObserver: Ember.observer('model.runningPaymentsPlanInterest',  'model.runningPaymentsPlanPrincipal',   ->
    Ember.run.once(this, 'updateYrsGraph')
  )


  initBaselineGraph: ->
    labels = []
    data = [@get('model.term')]

    yrsChartData = {
      labels: labels
      datasets: [
        {
          label: 'Baseline'
          data: data
          borderColor: 'rgba(157, 207, 242, 1.00)'
          backgroundColor: 'rgba(157, 207, 242, 0.75)'
        }, {
          label: 'Plan'
          data: data
          borderColor: 'rgba(72, 160, 220, 1)'
          backgroundColor: 'rgba(72, 160, 220, 0.75)'
        }
      ]
    }

    if @get('print')
      ctx = document.getElementById("yearsSavedChartPrint").getContext("2d")
    else
      ctx = document.getElementById("yearsSavedChart").getContext("2d")
    yrsChart = new Chart(ctx,
      type: 'bar'
      data: yrsChartData
      options:
        stacked: false
        responsive: true
        maintainAspectRatio: true
        scales: {
          xAxes: [
            categoryPercentage: 0.8
            stacked: false
          ]
          yAxes: [
            type: 'linear'
            ticks:
              beginAtZero: true
            scaleLabel:
              display: false
            beginAtZero: true
            stacked: false

          ]
        }
        legend:
          display: false
        tooltips:
          mode: 'label'
          callbacks:
            title: (tooltipItem, data) ->
              return 'Total term (yrs)'


    )
    @set 'yrsChart', yrsChart


  updateYrsGraph: ->
    baselineYears = @get('baselineYears')
    planYears = @get('planYears')

    yrsChart = @get 'yrsChart'
    labels = []
    i = 0

    yrsChart.data.labels = labels
    yrsChart.data.datasets[0].data = [baselineYears]
    yrsChart.data.datasets[1].data = [planYears]
    yrsChart.update()


)

`export default ToolsPlannerPlannerGraphYearsSavedComponent`
