`import Ember from 'ember'`

ToolsPlannerPlannerGraphEventIconComponent = Ember.Component.extend(

  classNames: ['planner-graph-event']

  attributeBindings: ['style', 'draggable']
  draggable: 'true'
  yearOver: null
  year: Ember.computed.alias('model.years.firstObject')
  index: Ember.computed.alias('model.yearIndex')

  style: Ember.computed('plan.runningBalancesPlan.[]', 'plan.events.[]', ->
    lineChart = @get 'lineChart'
    year = @get 'year'
    if lineChart && year
      index = @get 'index' || 0
      meta = lineChart.getDatasetMeta(0)
      x = meta.data[year]._model.x + (index * 5)
      y = meta.data[year]._model.y - 25 - (index * 15)
      z = 100 - index
      return Ember.String.htmlSafe("top: #{y}px; left: #{x}px; z-index: #{z}")
    )

  willDestroyElement: ->
    @_super()

  click: ->
    model = @get('model')
    @attrs.showEvent(model)


  dragStart: (e) ->
    @pickupIcon(e)
    return e

  touchStart: (e) ->
    @pickupIcon(e)


  drag: (e) ->
    @dragIcon(e)

  dragEnd: (e) ->
    @dropIcon(e)


  touchMove: (e) ->
    e.preventDefault()
    @dragIcon(e)

  touchEnd: (e) ->
    @dropIcon(e)




  pickupIcon: (e) ->
    #set our point of origin:
    if e.originalEvent.targetTouches
      clientX = e.originalEvent.targetTouches[0].clientX
      clientY = e.originalEvent.targetTouches[0].clientY
    else
      clientX = e.originalEvent.clientX
      clientY = e.originalEvent.clientY
    @set 'touchOrigin', {x: clientX, y:clientY}
    #Stop the ease-in-out delay:
    transition = "all 0s"
    transform = "translate(0px,0px)"
    @utilTransformIconCss(transition, transform)



  dragIcon: (e) ->
    #move the icon with the mouse/touch:
    if e.originalEvent.touches
      clientX = e.originalEvent.touches[0].clientX
      clientY = e.originalEvent.touches[0].clientY
    else
      clientX = e.originalEvent.clientX
      clientY = e.originalEvent.clientY
    if clientX == 0 and clientY == 0
      return false
    xChange = clientX - @get('touchOrigin').x
    yChange = clientY - @get('touchOrigin').y
    transform = "translate(#{xChange}px,#{yChange}px)"
    transition = "all 0s"
    @utilTransformIconCss(transition, transform)
    #Get what year we are over and display it:
    yearAtEvent = @utilGetYearAtEvent(e)
    maxYears = @get('maxYears')
    if yearAtEvent > 0 && yearAtEvent <= maxYears + 1
      @set 'yearOver', yearAtEvent
    else
      @set 'yearOver', null


  dropIcon: (e) ->
    #get the year we are over and move the event there:
    yearAtEvent = @get('yearOver')
    maxYears = @get('maxYears')
    if yearAtEvent > 0 && yearAtEvent <= maxYears
      loanPlanEvent = @get('model')
      loanPlanEvent.set('years', [yearAtEvent])
      loanPlanEvent.manageRecurringEvents()
    #remove the adjustment as we're at our new location
    changeString = "translate(0px,0px) scale(1)"
    transition = "all 0.3s ease-in-out"
    @utilTransformIconCss(changeString)
    @set 'yearOver', null


  utilGetYearAtEvent: (e) ->
    lineChart = @get('lineChart')
    yAxis0 = lineChart.scales['y-axis-0'].right + 23
    xAxisWidth = lineChart.scales['x-axis-0'].width
    yearsOnX = lineChart.scales['x-axis-0'].ticks.length - 1
    pxPerYear = xAxisWidth / yearsOnX

    @set('maxYears', yearsOnX)
    if e.originalEvent.changedTouches
      xLocation = e.originalEvent.changedTouches[0].clientX
    else
      xLocation = e.originalEvent.clientX
    yearAtEvent = Math.round((xLocation - yAxis0) / pxPerYear)
    return yearAtEvent


  utilTransformIconCss: (transition, transform) ->
    thisIconId = @get('elementId')
    $('#' + thisIconId).css('transition':transition)
    $('#' + thisIconId).css('-webkit-transition':transition)
    $('#' + thisIconId).css('-ms-transition':transition)
    $('#' + thisIconId).css('transform':transform)
    $('#' + thisIconId).css('-webkit-transform':transform)
    $('#' + thisIconId).css('-ms-transform':transform)


)

`export default ToolsPlannerPlannerGraphEventIconComponent`
