`import Ember from 'ember'`

ToolsPlannerPlannerYearDetailsComponent = Ember.Component.extend(

  classNames: ['s24']

  baselineBalance: Ember.computed('model.runningBalancesBaseline.[]', 'year', ->
    year = @get('year')
    return @get('model.runningBalancesBaseline').objectAt(year)
    )

  planBalance: Ember.computed('model.runningBalancesPlan.[]', 'year', ->
    year = @get('year')
    return @get('model.runningBalancesPlan').objectAt(year)
    )

  planPayment: Ember.computed('model.runningPaymentsPlan.[]', 'year', ->
    year = @get('year')
    return @get('model.runningPaymentsPlan').objectAt(year)
    )

  thisYearsEvents: Ember.computed('model.events.[]', 'year', ->
    @get('model.events').filterBy('year', @get('year'))
    )

  actions:
    addEvent: ->
      $(document).scrollTop()
      @set 'addEvent', true

    viewEvent: (event) ->
      $(document).scrollTop()
      @set 'selectedEvent', event
      @set 'viewEvent', true
      ###
      model = @get('model')
      newEvent = model.addEvent()
      newEvent.set('isInEditMode', true)
      ###


)

`export default ToolsPlannerPlannerYearDetailsComponent`
