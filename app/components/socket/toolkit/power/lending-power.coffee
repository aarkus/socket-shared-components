`import Ember from 'ember'`

ToolkitPowerLendingPowerComponent = Ember.Component.extend(

  session: Ember.inject.service('session')
  classNames: ['s24 flex-row-wrap flex-align-start flex-justify-centre whitesmoke-bg pricing-comparator-body']

  init: ->
    @_super()
    unless @get('session.savedTool')
      @set('showInstructions', true)
)

`export default ToolkitPowerLendingPowerComponent`
