`import Ember from 'ember'`

ToolsToolkitLoanStructureTableComponent = Ember.Component.extend(

  classNames: ['presentation-object-inner s24 white-bg']
  paymentFrequency: null

  pmtIsWeekly: Ember.computed.equal('paymentFrequency.name', 'Weekly')
  pmtIsFortnightly: Ember.computed.equal('paymentFrequency.name', 'Fortnightly')
  pmtIsMonthly: Ember.computed.equal('paymentFrequency.name', 'Monthly')


#used to let a pmt-calc component know its the only loan
  singleLoanStructure: Ember.computed.equal('model.length', 1)

#For the pmts display:
  weeklyPayments: Ember.computed.mapBy('model', 'weeklyPayment')
  fortnightlyPayments: Ember.computed.mapBy('model', 'fortnightlyPayment')
  monthlyPayments: Ember.computed.mapBy('model', 'monthlyPayment')
  totalWeeklyPayments: Ember.computed.sum('weeklyPayments')
  totalFortnightlyPayments: Ember.computed.sum('fortnightlyPayments')
  totalMonthlyPayments: Ember.computed.sum('monthlyPayments')


#for the proposition display
  propositionValue: Ember.computed('model.@each.owing', ->
    return 0 unless @get('model.length') > 0
    values = @get('model').mapBy('owing')
    totalValue = values.compact().reduce ((a, b) ->
      (parseFloat(a) + parseFloat(b)).toFixed(2)
      ), 0
    return totalValue
    )

  propositionInterestRate: Ember.computed('model.@each.owing', 'model.@each.interestRate', ->
    return 0 unless @get('model.length') > 0
    proposition = parseFloat(@get('propositionValue'))
    intRate = 0
    @get('model').forEach (loan) ->
      thisIntRate = parseFloat(loan.get('interestRate'))
      thisLoanAmount = parseFloat(loan.get('owing'))
      intRate += (thisLoanAmount/proposition) * thisIntRate
    return intRate || 0
    )


#observers:
  loanFrequenciesObs: Ember.observer('model.@each.frequency', ->
    differentFreqs = @get('model').mapBy('frequency').uniq().compact()
    if differentFreqs.length == 1
      @set 'paymentFrequency', differentFreqs[0]
    )




  actions:
    addLoan: ->
      monthlyFrequency = @store.peekAll('frequency').findBy('name', 'Monthly')
      newLoan = @store.createRecord('loanStructureLiability',
        owing: 100000
        interestRate: 0.05
        remainingMonths: 360
        frequency: monthlyFrequency
      )
      @model.pushObject(newLoan)
      Ember.run.later(this, ->
        newLoan.set 'showLoan', true
      , 200)

    close: ->
      @set 'closeAttribute', false

    removeLoan: (loan) ->
      loan.destroyRecord()

    resetPlan: ->
      loanPlanEvents = @get('loanPlan.events').toArray()
      loanPlanEvents.forEach (event) ->
        event.destroyRecord()


    selectPaymentFreq: (freq) ->
      frequency = @store.peekAll('frequency').findBy('name', freq)
      @set('paymentFrequency', frequency)

    showLoan: (loan) ->
      loan.set 'showLoan', true


  init: ->
    @_super()
    @send('selectPaymentFreq', 'Monthly')


)

`export default ToolsToolkitLoanStructureTableComponent`
