`import Ember from 'ember'`

ToolkitPiecesLoanDetailsSlidersComponent = Ember.Component.extend(

  classNames: ['flex-column-nowrap']

  interestTypes: ['P & I', 'Int only']

  interestTypeObs: Ember.observer('interestType', ->
    if @get('interestType') == 'Int only'
      @set('model.interestOnly', true)
    else
      @set('model.interestOnly', false)
    )

  init: ->
    @_super()
    @set 'interestType', @get('interestTypes.firstObject')
)

`export default ToolkitPiecesLoanDetailsSlidersComponent`
