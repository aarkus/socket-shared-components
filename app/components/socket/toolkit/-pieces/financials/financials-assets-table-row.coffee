`import Ember from 'ember'`
`import TableRowOperationsMixin from 'broker-crm/mixins/table-row-operations-mixin'`

FinancialsAssetsTableRowComponent = Ember.Component.extend(TableRowOperationsMixin,

    forex: false

    init: ->
      @set('assetTypes', @store.peekAll('assetType'))
      @_super()

)

`export default FinancialsAssetsTableRowComponent`
