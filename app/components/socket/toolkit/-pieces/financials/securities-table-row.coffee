`import Ember from 'ember'`
`import TableRowOperationsMixin from 'broker-crm/mixins/table-row-operations-mixin'`

SecuritiesTableRowComponent = Ember.Component.extend(TableRowOperationsMixin,

  forex: false

  init: ->
    @set('securityTypes', @store.peekAll('securityType'))
    @_super()


)

`export default SecuritiesTableRowComponent`
