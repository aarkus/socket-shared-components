`import Ember from 'ember'`
`import TableRowOperationsMixin from '../../../../mixins/shared/table-row-operations-mixin'`

FinancialsIncomesTableRowComponent = Ember.Component.extend(TableRowOperationsMixin,

  forex: false

  init:->
    @set('frequencies', @store.peekAll('frequency'))
    @set('incomeTypes', @store.peekAll('incomeType'))
    @_super()

  actions:
    saveEdit: ->
      @toggleProperty 'model.isInEditMode'
      return false

    deleteIncome: ->
      model = @get('model')
      @attrs.deleteIncome(model)
      return false

)

`export default FinancialsIncomesTableRowComponent`
