`import Ember from 'ember'`

ViewsFinancialsFinancialTableOuterContainerComponent = Ember.Component.extend(

  ###
  Needs:
    showTable: (bool) to match to with table row components for expand/contract
    optionTypes: (array) a record array of types of things that can be added (for the add button)
    addItem: (action) the action that gets called to add something to the list. The table doesn't do it
    bgColor: (class name) the class name that has the bgcolor for table header and add item popover
    label: (string) for the table header

  Options:
    counter: (array.length) for the header
    summaryValue: (value) to display $$val to the right of the header
  ###
  classNames: ['element-table']

  header: Ember.computed('counter', 'label', ->
    if @get('counter')
      return "#{@get('counter')} #{@get('label')}"
    else
      return @get 'label'
    )


  actions:
    toggleTable: ->
      @toggleProperty 'showTable'

    addItem: (type) ->
      @sendAction('action', type)

)

`export default ViewsFinancialsFinancialTableOuterContainerComponent`
