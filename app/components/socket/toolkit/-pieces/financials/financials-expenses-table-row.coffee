`import Ember from 'ember'`
`import TableRowOperationsMixin from '../../../../mixins/shared/table-row-operations-mixin'`

FinancialsExpensesTableRowComponent = Ember.Component.extend(TableRowOperationsMixin,

  forex: false

  init: ->
    @set('frequencies', @store.peekAll('frequency'))
    @set('expenseTypes', @store.peekAll('expenseType'))
    @_super()

  actions:
    saveEdit: ->
      @toggleProperty 'model.isInEditMode'
      return false

    deleteExpense: ->
      model = @get('model')
      @attrs.deleteExpense(model)
      return false


)

`export default FinancialsExpensesTableRowComponent`
