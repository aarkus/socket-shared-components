`import Ember from 'ember'`
`import TableRowOperationsMixin from 'broker-crm/mixins/table-row-operations-mixin'`

FinancialsLiabilitiesTableRowComponent = Ember.Component.extend(TableRowOperationsMixin,

  forex: false

  init: ->
    @set('liabilityTypes', @store.peekAll('liabilityType'))
    @_super()
)

`export default FinancialsLiabilitiesTableRowComponent`
