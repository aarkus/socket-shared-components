`import Ember from 'ember'`

ToolkitPiecesLoanListingComponent = Ember.Component.extend(

  classNames: ['s24 flex-column-nowrap']

  actions:

    removeLoan: (loan) ->
      @get('model').removeObject(loan)


    showLoan: (loan) ->
      loan.set 'showLoan', true

)

`export default ToolkitPiecesLoanListingComponent`
