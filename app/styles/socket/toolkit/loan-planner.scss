.planner-graph-canvas {
  width: 100%;

  &.balance{
    height: calc(((100vh - 5rem - 150px - 5vh) / 3) * 2) !important;
    min-height: 300px;
  }

  &.balance-print {
    width: 740px;
    height: 300px !important;
  }

  &.payment {
    height: calc(((100vh - 5rem - 150px - 5vh) / 3) * 1) !important;
    min-height: 150px;
  }

  &.payment-print {
    width: 745px !important;
    height: 230px !important;
  }

  &.donut-print {
    width: 230px !important;
    height: 230px !important;
  }

  &.years-print {
    width: 230px !important;
    height: 230px !important;
  }


}

.planner-graph-drag-here-overlay {
  position: absolute;
  z-index: 100;
  top: 0px;
  left: 0px;
  width: 100%;
  height: calc(((100vh - 5rem - 150px - 5vh) / 3) * 2) !important;
  min-height: 300px;
  background-color: rgba(255,255,255,0.5);
  align-items: center;
  align-content: center;
  justify-content: center;
  text-align: center;
  display: flex;
}


.loan-planner-graph-overlay{
  position: absolute;
  z-index: 101;
  top: -5px;
  right: -10px;
  width: 260px;
  height: 135px;
  background-color: white;
  border: 3px solid rgba(220, 220, 220, 0.5);
  border-radius: 3px;
  overflow: hidden;
  @include easer(0.4s);
  @include boxShadow();

  &.enlarged {
    height: 205px;
  }

  &.reduced {
    width: 30px;
    height: 30px;
    border-radius: 100%;
  }
}

.planner-events-container {
  padding: 10px 0;
  border-bottom: 1px solid rgba(220,220,220,0.5);
  @include flexbox();
  @include flex-flow(row nowrap);
  @include justify-content(space-around);
}

.planner-event {
  background-color: rgba(0, 0, 0, 0);
  //@include flexbox();
  @include flex-flow(column nowrap);
  @include align-items(center);
  @include align-content(center);
  @include flex(1 0 1%);
}

.planner-event-type-label {
  font-size: 0.6rem;
  text-align: center;

}

.planner-event-year-over-label {
  position: absolute;
  left: -10px;
  width: 50px;
  top: -1.5rem;
  font-size: 1.2rem;
}

.planner-graph-event {
  position: absolute;
  height: 25px;
  width: 25px;
  cursor: pointer;
  @include easer(0.4s);
  &:hover {
    @include scale(1.25);
  }
}
/*
.planner-balance-payment-container {
  padding: 5px;
  min-height: 700px;
  @include flexbox();
}
*/
.planner-donut-container {
  padding: 5px;
  //max-height: 150px;
  @include flexbox();
  @include flex-flow(row wrap);
  @include respond-to(medium) {
    //height: calc(100% - 10px);
    @include flex-flow(column nowrap);

  };
}

.savings-chart {
  width: 100%;
  height: 200px;
}

.planner-savings-box {
  width: 96%;
  margin:2%;
  padding: 5px;
  background-color: $accentDarkest;
  color: white;
  //max-height: 200px;
  //align-self: flex-end;
  flex: 1 0 auto;
  justify-content: center;
  align-items: center;
  align-content: center;
  display: flex;
  flex-flow: column nowrap;
  border-radius: 3px;

  &.positive {
    background-color: $icongreen;
  }

  &.negative {
    background-color: $iconred;
  }

}

.planner-proposition-bubble {
  //width: 120px;
  border-radius: 3px 3px 0 0;
  background-color: $accentDarker;
  color: white;
  padding: 6px 12px;
  font-size: 15px;
  opacity: 0.9;
  height: 46px;
  cursor: pointer;
  text-align: center;
  //z-index: 999;
  margin-left: 5px;
  @include boxShadow();


  &:hover {
    opacity: 1
  };

}

.planner-proposition-container {
  position: fixed;
  width: 320px;
  max-width: 95%;
  left: 5px;
  bottom: 0px;
  transform: translateY(calc(100% - 46px));
  transition: all 0.4s ease-in-out;

  //max-height: 0vh;
  overflow: hidden;

  //opacity: 0;
  z-index: 1001;
  //@include easer(0.4s);
  @include flex-flow(column nowrap);
  @include align-items(center);
  @include align-content(center);
  //@include scale(0);

  &.in-focus {
    transform: translateY(0);
    //max-height: 100vh;
    //@include scale(1);
  }
}

.planner-message-alert {
  position: fixed;
  right: 8px;
  bottom: 5px;
  cursor: pointer;
  &:hover {
    opacity: 0.8;
  }

}

.embolden {
  transition: all 0.2s ease-in-out;
  transform: scale(1.3);
  border-bottom: 2px solid #2789CF;
}

.instructions-close-button {
  position: absolute;
  right: 5px;
  top: 5px;
  border: none;
  cursor: pointer;
  background-color: transparent;
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  align-items: center;
  align-content: center;
}


.planner-print-container {

  &.line-graph {
    width: 750px;
    height: 300px;
    max-height: 300px;
  }
  &.payment-graph {
    width: 750px;
    height: 250px;
    max-height: 250px;
  }

  &.donut-graph {
    width: 230px;
    height: 230px;

  }

  &.years-graph {
    width: 230px;
    height: 230px;
  }
}
