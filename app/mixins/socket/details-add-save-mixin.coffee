`import Ember from 'ember'`

DetailsAddSaveMixin = Ember.Mixin.create(

  session: Ember.inject.service('session')

  classNames: ['s24 flex-column-nowrap flex-justify-start flex-align-centre']


  actions:
    saveRecord:  ->
      self = @
      model = @get('model.content') || @get('model')
      @set('isSaving', true)
      model.save().then ->
        self.set('isSaving', false)
        self.set('closeAttribute', false)
        return false


    deleteRecord: ->
      model = @get('model.content') || @get('model')
      loanApplication = @get('loanApplication')
      unless model.get('id')
        model.deleteRecord()
        return false
      if model.get('id').length > 10
        model.deleteRecord()
      else
        model.destroyRecord()
      return false


    undoChanges: ->
      model = @get('model.content') || @get('model')
      model.rollbackAttributes()
      return false




  addNextRecord: ->
    @attrs.addNextRecord()
    return false


)



`export default DetailsAddSaveMixin`
